import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private http: HttpClient) { }

  getDataJson(){
    let apiUrl = "/api/get-data-json?country=brazil";
    return this.http.get(apiUrl)
  }
}
