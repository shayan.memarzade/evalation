import { Component, OnInit  } from '@angular/core';
import {ApiServiceService} from './api-service.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  chartData;
  countries;
  countryData;
  newCountryData;
  allCountryData;
  filterForm;
  selectedCountry;
  secondCountryData;
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'A' },
  ];
  public lineChartLabels: Label[] = [];
  public lineChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];


  constructor(
    private apiServiceService: ApiServiceService,
    private formBuilder: FormBuilder) { }
  ngOnInit() {

    this.filterForm = this.formBuilder.group({
      startDate: '',
      endDate: '',
      country: ''
    });
    
    // this.apiServiceService.getDataJson().subscribe(data => this.chartData = data)
    this.countries = {
      "brazil": 6,
      "belarus": 11
    }
    this.chartData = [
      {
          "InflationRate": 0.0934579439252336448598130841,
          "InflationRateRounded": 0.09,
          "InflationRateFormatted": "0.09",
          "Month": "/Date(1543622400000)/",
          "MonthFormatted": "2018-12-01",
          "Country": 7
      },
      {
          "InflationRate": 0.2811621368322399250234301781,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(1541030400000)/",
          "MonthFormatted": "2018-11-01",
          "Country": 7
      },
      {
          "InflationRate": 0.0938086303939962476547842402,
          "InflationRateRounded": 0.09,
          "InflationRateFormatted": "0.09",
          "Month": "/Date(1538352000000)/",
          "MonthFormatted": "2018-10-01",
          "Country": 7
      },
      {
          "InflationRate": 0.0938967136150234741784037559,
          "InflationRateRounded": 0.09,
          "InflationRateFormatted": "0.09",
          "Month": "/Date(1535760000000)/",
          "MonthFormatted": "2018-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6616257088846880907372400756,
          "InflationRateRounded": 0.66,
          "InflationRateFormatted": "0.66",
          "Month": "/Date(1533081600000)/",
          "MonthFormatted": "2018-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1530403200000)/",
          "MonthFormatted": "2018-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1527811200000)/",
          "MonthFormatted": "2018-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3795066413662239089184060721,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(1525132800000)/",
          "MonthFormatted": "2018-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.380952380952380952380952381,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(1522540800000)/",
          "MonthFormatted": "2018-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0953288846520495710200190658,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1519862400000)/",
          "MonthFormatted": "2018-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4789272030651340996168582375,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(1517443200000)/",
          "MonthFormatted": "2018-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4766444232602478551000953289,
          "InflationRateRounded": -0.48,
          "InflationRateFormatted": "-0.48",
          "Month": "/Date(1514764800000)/",
          "MonthFormatted": "2018-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2868068833652007648183556405,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(1512086400000)/",
          "MonthFormatted": "2017-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3838771593090211132437619962,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(1509494400000)/",
          "MonthFormatted": "2017-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.096061479346781940441882805,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1506816000000)/",
          "MonthFormatted": "2017-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2890173410404624277456647399,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(1504224000000)/",
          "MonthFormatted": "2017-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5813953488372093023255813953,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(1501545600000)/",
          "MonthFormatted": "2017-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0968054211035818005808325266,
          "InflationRateRounded": -0.10,
          "InflationRateFormatted": "-0.10",
          "Month": "/Date(1498867200000)/",
          "MonthFormatted": "2017-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1496275200000)/",
          "MonthFormatted": "2017-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3887269193391642371234207969,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1493596800000)/",
          "MonthFormatted": "2017-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.390243902439024390243902439,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1491004800000)/",
          "MonthFormatted": "2017-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3917727717923604309500489716,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1488326400000)/",
          "MonthFormatted": "2017-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6903353057199211045364891519,
          "InflationRateRounded": 0.69,
          "InflationRateFormatted": "0.69",
          "Month": "/Date(1485907200000)/",
          "MonthFormatted": "2017-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.490677134445534838076545633,
          "InflationRateRounded": -0.49,
          "InflationRateFormatted": "-0.49",
          "Month": "/Date(1483228800000)/",
          "MonthFormatted": "2017-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4930966469428007889546351085,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(1480550400000)/",
          "MonthFormatted": "2016-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1976284584980237154150197628,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1477958400000)/",
          "MonthFormatted": "2016-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0989119683481701285855588526,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1475280000000)/",
          "MonthFormatted": "2016-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1982160555004955401387512389,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1472688000000)/",
          "MonthFormatted": "2016-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2982107355864811133200795229,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(1470009600000)/",
          "MonthFormatted": "2016-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1467331200000)/",
          "MonthFormatted": "2016-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1992031872509960159362549801,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1464739200000)/",
          "MonthFormatted": "2016-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1996007984031936127744510978,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1462060800000)/",
          "MonthFormatted": "2016-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1459468800000)/",
          "MonthFormatted": "2016-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4008016032064128256513026052,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1456790400000)/",
          "MonthFormatted": "2016-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3015075376884422110552763819,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(1454284800000)/",
          "MonthFormatted": "2016-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.7976071784646061814556331007,
          "InflationRateRounded": -0.80,
          "InflationRateFormatted": "-0.80",
          "Month": "/Date(1451606400000)/",
          "MonthFormatted": "2016-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1448928000000)/",
          "MonthFormatted": "2015-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1446336000000)/",
          "MonthFormatted": "2015-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0998003992015968063872255489,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1443657600000)/",
          "MonthFormatted": "2015-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0997008973080757726819541376,
          "InflationRateRounded": -0.10,
          "InflationRateFormatted": "-0.10",
          "Month": "/Date(1441065600000)/",
          "MonthFormatted": "2015-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3,
          "InflationRateRounded": 0.3,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(1438387200000)/",
          "MonthFormatted": "2015-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1996007984031936127744510978,
          "InflationRateRounded": -0.20,
          "InflationRateFormatted": "-0.20",
          "Month": "/Date(1435708800000)/",
          "MonthFormatted": "2015-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0999000999000999000999000999,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1433116800000)/",
          "MonthFormatted": "2015-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2002002002002002002002002002,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1430438400000)/",
          "MonthFormatted": "2015-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2006018054162487462387161484,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1427846400000)/",
          "MonthFormatted": "2015-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2010050251256281407035175879,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1425168000000)/",
          "MonthFormatted": "2015-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2014098690835850956696878147,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1422748800000)/",
          "MonthFormatted": "2015-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.7992007992007992007992007992,
          "InflationRateRounded": -0.80,
          "InflationRateFormatted": "-0.80",
          "Month": "/Date(1420070400000)/",
          "MonthFormatted": "2015-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1417392000000)/",
          "MonthFormatted": "2014-12-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2988047808764940239043824701,
          "InflationRateRounded": -0.30,
          "InflationRateFormatted": "-0.30",
          "Month": "/Date(1414800000000)/",
          "MonthFormatted": "2014-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0997008973080757726819541376,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1412121600000)/",
          "MonthFormatted": "2014-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0998003992015968063872255489,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1409529600000)/",
          "MonthFormatted": "2014-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3003003003003003003003003003,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(1406851200000)/",
          "MonthFormatted": "2014-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2994011976047904191616766467,
          "InflationRateRounded": -0.30,
          "InflationRateFormatted": "-0.30",
          "Month": "/Date(1404172800000)/",
          "MonthFormatted": "2014-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2,
          "InflationRateRounded": 0.2,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1401580800000)/",
          "MonthFormatted": "2014-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0999000999000999000999000999,
          "InflationRateRounded": -0.10,
          "InflationRateFormatted": "-0.10",
          "Month": "/Date(1398902400000)/",
          "MonthFormatted": "2014-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4012036108324974924774322969,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1396310400000)/",
          "MonthFormatted": "2014-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2010050251256281407035175879,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1393632000000)/",
          "MonthFormatted": "2014-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5050505050505050505050505051,
          "InflationRateRounded": 0.51,
          "InflationRateFormatted": "0.51",
          "Month": "/Date(1391212800000)/",
          "MonthFormatted": "2014-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6024096385542168674698795181,
          "InflationRateRounded": -0.60,
          "InflationRateFormatted": "-0.60",
          "Month": "/Date(1388534400000)/",
          "MonthFormatted": "2014-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4032258064516129032258064516,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1385856000000)/",
          "MonthFormatted": "2013-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1009081735620585267406659939,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1383264000000)/",
          "MonthFormatted": "2013-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1380585600000)/",
          "MonthFormatted": "2013-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4052684903748733535967578521,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(1377993600000)/",
          "MonthFormatted": "2013-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4069175991861648016276703967,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(1375315200000)/",
          "MonthFormatted": "2013-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1372636800000)/",
          "MonthFormatted": "2013-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2030456852791878172588832487,
          "InflationRateRounded": -0.20,
          "InflationRateFormatted": "-0.20",
          "Month": "/Date(1370044800000)/",
          "MonthFormatted": "2013-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2034587995930824008138351984,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1367366400000)/",
          "MonthFormatted": "2013-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2038735983690112130479102956,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(1364774400000)/",
          "MonthFormatted": "2013-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3067484662576687116564417178,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(1362096000000)/",
          "MonthFormatted": "2013-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7209062821833161688980432544,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(1359676800000)/",
          "MonthFormatted": "2013-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5122950819672131147540983607,
          "InflationRateRounded": -0.51,
          "InflationRateFormatted": "-0.51",
          "Month": "/Date(1356998400000)/",
          "MonthFormatted": "2013-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4115226337448559670781893004,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(1354320000000)/",
          "MonthFormatted": "2012-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2061855670103092783505154639,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(1351728000000)/",
          "MonthFormatted": "2012-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5181347150259067357512953368,
          "InflationRateRounded": 0.52,
          "InflationRateFormatted": "0.52",
          "Month": "/Date(1349049600000)/",
          "MonthFormatted": "2012-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4162330905306971904266389178,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(1346457600000)/",
          "MonthFormatted": "2012-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5230125523012552301255230126,
          "InflationRateRounded": 0.52,
          "InflationRateFormatted": "0.52",
          "Month": "/Date(1343779200000)/",
          "MonthFormatted": "2012-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1047120418848167539267015707,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(1341100800000)/",
          "MonthFormatted": "2012-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4171011470281543274244004171,
          "InflationRateRounded": -0.42,
          "InflationRateFormatted": "-0.42",
          "Month": "/Date(1338508800000)/",
          "MonthFormatted": "2012-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1041666666666666666666666667,
          "InflationRateRounded": -0.10,
          "InflationRateFormatted": "-0.10",
          "Month": "/Date(1335830400000)/",
          "MonthFormatted": "2012-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6289308176100628930817610063,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(1333238400000)/",
          "MonthFormatted": "2012-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3154574132492113564668769716,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(1330560000000)/",
          "MonthFormatted": "2012-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5285412262156448202959830867,
          "InflationRateRounded": 0.53,
          "InflationRateFormatted": "0.53",
          "Month": "/Date(1328054400000)/",
          "MonthFormatted": "2012-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.525762355415352260778128286,
          "InflationRateRounded": -0.53,
          "InflationRateFormatted": "-0.53",
          "Month": "/Date(1325376000000)/",
          "MonthFormatted": "2012-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5285412262156448202959830867,
          "InflationRateRounded": 0.53,
          "InflationRateFormatted": "0.53",
          "Month": "/Date(1322697600000)/",
          "MonthFormatted": "2011-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1058201058201058201058201058,
          "InflationRateRounded": 0.11,
          "InflationRateFormatted": "0.11",
          "Month": "/Date(1320105600000)/",
          "MonthFormatted": "2011-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1059322033898305084745762712,
          "InflationRateRounded": 0.11,
          "InflationRateFormatted": "0.11",
          "Month": "/Date(1317427200000)/",
          "MonthFormatted": "2011-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6396588486140724946695095949,
          "InflationRateRounded": 0.64,
          "InflationRateFormatted": "0.64",
          "Month": "/Date(1314835200000)/",
          "MonthFormatted": "2011-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5359056806002143622722400857,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(1312156800000)/",
          "MonthFormatted": "2011-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1309478400000)/",
          "MonthFormatted": "2011-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1070663811563169164882226981,
          "InflationRateRounded": -0.11,
          "InflationRateFormatted": "-0.11",
          "Month": "/Date(1306886400000)/",
          "MonthFormatted": "2011-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.214592274678111587982832618,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(1304208000000)/",
          "MonthFormatted": "2011-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.084598698481561822125813449,
          "InflationRateRounded": 1.08,
          "InflationRateFormatted": "1.08",
          "Month": "/Date(1301616000000)/",
          "MonthFormatted": "2011-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2173913043478260869565217391,
          "InflationRateRounded": 0.22,
          "InflationRateFormatted": "0.22",
          "Month": "/Date(1298937600000)/",
          "MonthFormatted": "2011-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7667031763417305585980284775,
          "InflationRateRounded": 0.77,
          "InflationRateFormatted": "0.77",
          "Month": "/Date(1296518400000)/",
          "MonthFormatted": "2011-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1096491228070175438596491228,
          "InflationRateRounded": 0.11,
          "InflationRateFormatted": "0.11",
          "Month": "/Date(1293840000000)/",
          "MonthFormatted": "2011-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.996677740863787375415282392,
          "InflationRateRounded": 1.00,
          "InflationRateFormatted": "1.00",
          "Month": "/Date(1291161600000)/",
          "MonthFormatted": "2010-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3333333333333333333333333333,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(1288569600000)/",
          "MonthFormatted": "2010-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.222717149220489977728285078,
          "InflationRateRounded": 0.22,
          "InflationRateFormatted": "0.22",
          "Month": "/Date(1285891200000)/",
          "MonthFormatted": "2010-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1283299200000)/",
          "MonthFormatted": "2010-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5599104143337066069428891377,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(1280620800000)/",
          "MonthFormatted": "2010-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2234636871508379888268156425,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(1277942400000)/",
          "MonthFormatted": "2010-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1118568232662192393736017897,
          "InflationRateRounded": 0.11,
          "InflationRateFormatted": "0.11",
          "Month": "/Date(1275350400000)/",
          "MonthFormatted": "2010-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2242152466367713004484304933,
          "InflationRateRounded": 0.22,
          "InflationRateFormatted": "0.22",
          "Month": "/Date(1272672000000)/",
          "MonthFormatted": "2010-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5636978579481397970687711387,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(1270080000000)/",
          "MonthFormatted": "2010-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5668934240362811791383219955,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(1267401600000)/",
          "MonthFormatted": "2010-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4555808656036446469248291572,
          "InflationRateRounded": 0.46,
          "InflationRateFormatted": "0.46",
          "Month": "/Date(1264982400000)/",
          "MonthFormatted": "2010-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2272727272727272727272727273,
          "InflationRateRounded": -0.23,
          "InflationRateFormatted": "-0.23",
          "Month": "/Date(1262304000000)/",
          "MonthFormatted": "2010-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5714285714285714285714285714,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(1259625600000)/",
          "MonthFormatted": "2009-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3440366972477064220183486239,
          "InflationRateRounded": 0.34,
          "InflationRateFormatted": "0.34",
          "Month": "/Date(1257033600000)/",
          "MonthFormatted": "2009-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.114810562571756601607347876,
          "InflationRateRounded": 0.11,
          "InflationRateFormatted": "0.11",
          "Month": "/Date(1254355200000)/",
          "MonthFormatted": "2009-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.114942528735632183908045977,
          "InflationRateRounded": 0.11,
          "InflationRateFormatted": "0.11",
          "Month": "/Date(1251763200000)/",
          "MonthFormatted": "2009-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3460207612456747404844290657,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(1249084800000)/",
          "MonthFormatted": "2009-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1246406400000)/",
          "MonthFormatted": "2009-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3472222222222222222222222222,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(1243814400000)/",
          "MonthFormatted": "2009-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4651162790697674418604651163,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(1241136000000)/",
          "MonthFormatted": "2009-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2331002331002331002331002331,
          "InflationRateRounded": 0.23,
          "InflationRateFormatted": "0.23",
          "Month": "/Date(1238544000000)/",
          "MonthFormatted": "2009-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2336448598130841121495327103,
          "InflationRateRounded": 0.23,
          "InflationRateFormatted": "0.23",
          "Month": "/Date(1235865600000)/",
          "MonthFormatted": "2009-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8244994110718492343934040047,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(1233446400000)/",
          "MonthFormatted": "2009-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.701754385964912280701754386,
          "InflationRateRounded": -0.70,
          "InflationRateFormatted": "-0.70",
          "Month": "/Date(1230768000000)/",
          "MonthFormatted": "2009-01-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3496503496503496503496503497,
          "InflationRateRounded": -0.35,
          "InflationRateFormatted": "-0.35",
          "Month": "/Date(1228089600000)/",
          "MonthFormatted": "2008-12-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1164144353899883585564610012,
          "InflationRateRounded": -0.12,
          "InflationRateFormatted": "-0.12",
          "Month": "/Date(1225497600000)/",
          "MonthFormatted": "2008-11-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2322880371660859465737514518,
          "InflationRateRounded": -0.23,
          "InflationRateFormatted": "-0.23",
          "Month": "/Date(1222819200000)/",
          "MonthFormatted": "2008-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4667444574095682613768961494,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(1220227200000)/",
          "MonthFormatted": "2008-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7050528789659224441833137485,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(1217548800000)/",
          "MonthFormatted": "2008-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1173708920187793427230046948,
          "InflationRateRounded": -0.12,
          "InflationRateFormatted": "-0.12",
          "Month": "/Date(1214870400000)/",
          "MonthFormatted": "2008-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7092198581560283687943262411,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(1212278400000)/",
          "MonthFormatted": "2008-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7142857142857142857142857143,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(1209600000000)/",
          "MonthFormatted": "2008-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7194244604316546762589928058,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(1207008000000)/",
          "MonthFormatted": "2008-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4819277108433734939759036145,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(1204329600000)/",
          "MonthFormatted": "2008-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7281553398058252427184466019,
          "InflationRateRounded": 0.73,
          "InflationRateFormatted": "0.73",
          "Month": "/Date(1201824000000)/",
          "MonthFormatted": "2008-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.7228915662650602409638554217,
          "InflationRateRounded": -0.72,
          "InflationRateFormatted": "-0.72",
          "Month": "/Date(1199145600000)/",
          "MonthFormatted": "2008-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6060606060606060606060606061,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(1196467200000)/",
          "MonthFormatted": "2007-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2430133657351154313487241798,
          "InflationRateRounded": 0.24,
          "InflationRateFormatted": "0.24",
          "Month": "/Date(1193875200000)/",
          "MonthFormatted": "2007-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4884004884004884004884004884,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(1191196800000)/",
          "MonthFormatted": "2007-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1222493887530562347188264059,
          "InflationRateRounded": 0.12,
          "InflationRateFormatted": "0.12",
          "Month": "/Date(1188604800000)/",
          "MonthFormatted": "2007-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3680981595092024539877300613,
          "InflationRateRounded": 0.37,
          "InflationRateFormatted": "0.37",
          "Month": "/Date(1185926400000)/",
          "MonthFormatted": "2007-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.609756097560975609756097561,
          "InflationRateRounded": -0.61,
          "InflationRateFormatted": "-0.61",
          "Month": "/Date(1183248000000)/",
          "MonthFormatted": "2007-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2444987775061124694376528117,
          "InflationRateRounded": 0.24,
          "InflationRateFormatted": "0.24",
          "Month": "/Date(1180656000000)/",
          "MonthFormatted": "2007-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2450980392156862745098039216,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(1177977600000)/",
          "MonthFormatted": "2007-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2457002457002457002457002457,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(1175385600000)/",
          "MonthFormatted": "2007-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4938271604938271604938271605,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(1172707200000)/",
          "MonthFormatted": "2007-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4962779156327543424317617866,
          "InflationRateRounded": 0.50,
          "InflationRateFormatted": "0.50",
          "Month": "/Date(1170288000000)/",
          "MonthFormatted": "2007-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.8610086100861008610086100861,
          "InflationRateRounded": -0.86,
          "InflationRateFormatted": "-0.86",
          "Month": "/Date(1167609600000)/",
          "MonthFormatted": "2007-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6188118811881188118811881188,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(1164931200000)/",
          "MonthFormatted": "2006-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2481389578163771712158808933,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(1162339200000)/",
          "MonthFormatted": "2006-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1242236024844720496894409938,
          "InflationRateRounded": 0.12,
          "InflationRateFormatted": "0.12",
          "Month": "/Date(1159660800000)/",
          "MonthFormatted": "2006-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.124378109452736318407960199,
          "InflationRateRounded": 0.12,
          "InflationRateFormatted": "0.12",
          "Month": "/Date(1157068800000)/",
          "MonthFormatted": "2006-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5,
          "InflationRateRounded": 0.5,
          "InflationRateFormatted": "0.50",
          "Month": "/Date(1154390400000)/",
          "MonthFormatted": "2006-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1248439450686641697877652934,
          "InflationRateRounded": -0.12,
          "InflationRateFormatted": "-0.12",
          "Month": "/Date(1151712000000)/",
          "MonthFormatted": "2006-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2503128911138923654568210263,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(1149120000000)/",
          "MonthFormatted": "2006-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6297229219143576826196473552,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(1146441600000)/",
          "MonthFormatted": "2006-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6337135614702154626108998733,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(1143849600000)/",
          "MonthFormatted": "2006-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1269035532994923857868020305,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1141171200000)/",
          "MonthFormatted": "2006-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3821656050955414012738853503,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(1138752000000)/",
          "MonthFormatted": "2006-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5069708491761723700887198986,
          "InflationRateRounded": -0.51,
          "InflationRateFormatted": "-0.51",
          "Month": "/Date(1136073600000)/",
          "MonthFormatted": "2006-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2541296060991105463786531131,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(1133395200000)/",
          "MonthFormatted": "2005-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1130803200000)/",
          "MonthFormatted": "2005-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1272264631043256997455470738,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1128124800000)/",
          "MonthFormatted": "2005-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.255102040816326530612244898,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1125532800000)/",
          "MonthFormatted": "2005-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2557544757033248081841432225,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1122854400000)/",
          "MonthFormatted": "2005-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1280409731113956466069142125,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1120176000000)/",
          "MonthFormatted": "2005-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1117584000000)/",
          "MonthFormatted": "2005-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3856041131105398457583547558,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1114905600000)/",
          "MonthFormatted": "2005-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3870967741935483870967741935,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1112313600000)/",
          "MonthFormatted": "2005-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3886010362694300518134715026,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1109635200000)/",
          "MonthFormatted": "2005-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2597402597402597402597402597,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1107216000000)/",
          "MonthFormatted": "2005-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5167958656330749354005167959,
          "InflationRateRounded": -0.52,
          "InflationRateFormatted": "-0.52",
          "Month": "/Date(1104537600000)/",
          "MonthFormatted": "2005-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5194805194805194805194805195,
          "InflationRateRounded": 0.52,
          "InflationRateFormatted": "0.52",
          "Month": "/Date(1101859200000)/",
          "MonthFormatted": "2004-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1300390117035110533159947984,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1099267200000)/",
          "MonthFormatted": "2004-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2607561929595827900912646675,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1096588800000)/",
          "MonthFormatted": "2004-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1305483028720626631853785901,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1093996800000)/",
          "MonthFormatted": "2004-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2617801047120418848167539267,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1091318400000)/",
          "MonthFormatted": "2004-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2610966057441253263707571802,
          "InflationRateRounded": -0.26,
          "InflationRateFormatted": "-0.26",
          "Month": "/Date(1088640000000)/",
          "MonthFormatted": "2004-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1086048000000)/",
          "MonthFormatted": "2004-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2617801047120418848167539267,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1083369600000)/",
          "MonthFormatted": "2004-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3942181340341655716162943495,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1080777600000)/",
          "MonthFormatted": "2004-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1315789473684210526315789474,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1078099200000)/",
          "MonthFormatted": "2004-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2638522427440633245382585752,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1075593600000)/",
          "MonthFormatted": "2004-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.524934383202099737532808399,
          "InflationRateRounded": -0.52,
          "InflationRateFormatted": "-0.52",
          "Month": "/Date(1072915200000)/",
          "MonthFormatted": "2004-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3952569169960474308300395257,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1070236800000)/",
          "MonthFormatted": "2003-12-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1315789473684210526315789474,
          "InflationRateRounded": -0.13,
          "InflationRateFormatted": "-0.13",
          "Month": "/Date(1067644800000)/",
          "MonthFormatted": "2003-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1317523056653491436100131752,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1064966400000)/",
          "MonthFormatted": "2003-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3968253968253968253968253968,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1062374400000)/",
          "MonthFormatted": "2003-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3984063745019920318725099602,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1059696000000)/",
          "MonthFormatted": "2003-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1326259946949602122015915119,
          "InflationRateRounded": -0.13,
          "InflationRateFormatted": "-0.13",
          "Month": "/Date(1057017600000)/",
          "MonthFormatted": "2003-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1324503311258278145695364238,
          "InflationRateRounded": -0.13,
          "InflationRateFormatted": "-0.13",
          "Month": "/Date(1054425600000)/",
          "MonthFormatted": "2003-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1051747200000)/",
          "MonthFormatted": "2003-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2656042496679946879150066401,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(1049155200000)/",
          "MonthFormatted": "2003-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4,
          "InflationRateRounded": 0.4,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1046476800000)/",
          "MonthFormatted": "2003-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4016064257028112449799196787,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1044057600000)/",
          "MonthFormatted": "2003-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6648936170212765957446808511,
          "InflationRateRounded": -0.66,
          "InflationRateFormatted": "-0.66",
          "Month": "/Date(1041379200000)/",
          "MonthFormatted": "2003-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4005340453938584779706275033,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1038700800000)/",
          "MonthFormatted": "2002-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1036108800000)/",
          "MonthFormatted": "2002-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1336898395721925133689839572,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1033430400000)/",
          "MonthFormatted": "2002-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2680965147453083109919571046,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(1030838400000)/",
          "MonthFormatted": "2002-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2688172043010752688172043011,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(1028160000000)/",
          "MonthFormatted": "2002-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2680965147453083109919571046,
          "InflationRateRounded": -0.27,
          "InflationRateFormatted": "-0.27",
          "Month": "/Date(1025481600000)/",
          "MonthFormatted": "2002-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(1022889600000)/",
          "MonthFormatted": "2002-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2688172043010752688172043011,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(1020211200000)/",
          "MonthFormatted": "2002-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4048582995951417004048582996,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(1017619200000)/",
          "MonthFormatted": "2002-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4065040650406504065040650407,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(1014940800000)/",
          "MonthFormatted": "2002-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1356852103120759837177747626,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(1012521600000)/",
          "MonthFormatted": "2002-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4054054054054054054054054054,
          "InflationRateRounded": -0.41,
          "InflationRateFormatted": "-0.41",
          "Month": "/Date(1009843200000)/",
          "MonthFormatted": "2002-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2710027100271002710027100271,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(1007164800000)/",
          "MonthFormatted": "2001-12-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1353179972936400541271989175,
          "InflationRateRounded": -0.14,
          "InflationRateFormatted": "-0.14",
          "Month": "/Date(1004572800000)/",
          "MonthFormatted": "2001-11-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2699055330634278002699055331,
          "InflationRateRounded": -0.27,
          "InflationRateFormatted": "-0.27",
          "Month": "/Date(1001894400000)/",
          "MonthFormatted": "2001-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2706359945872801082543978349,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(999302400000)/",
          "MonthFormatted": "2001-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4076086956521739130434782609,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(996624000000)/",
          "MonthFormatted": "2001-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6747638326585695006747638327,
          "InflationRateRounded": -0.67,
          "InflationRateFormatted": "-0.67",
          "Month": "/Date(993945600000)/",
          "MonthFormatted": "2001-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1351351351351351351351351351,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(991353600000)/",
          "MonthFormatted": "2001-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8174386920980926430517711172,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(988675200000)/",
          "MonthFormatted": "2001-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5479452054794520547945205479,
          "InflationRateRounded": 0.55,
          "InflationRateFormatted": "0.55",
          "Month": "/Date(986083200000)/",
          "MonthFormatted": "2001-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4126547455295735900962861073,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(983404800000)/",
          "MonthFormatted": "2001-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1377410468319559228650137741,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(980985600000)/",
          "MonthFormatted": "2001-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.819672131147540983606557377,
          "InflationRateRounded": -0.82,
          "InflationRateFormatted": "-0.82",
          "Month": "/Date(978307200000)/",
          "MonthFormatted": "2001-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(975628800000)/",
          "MonthFormatted": "2000-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1367989056087551299589603283,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(973036800000)/",
          "MonthFormatted": "2000-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(970358400000)/",
          "MonthFormatted": "2000-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8275862068965517241379310345,
          "InflationRateRounded": 0.83,
          "InflationRateFormatted": "0.83",
          "Month": "/Date(967766400000)/",
          "MonthFormatted": "2000-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(965088000000)/",
          "MonthFormatted": "2000-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5486968449931412894375857339,
          "InflationRateRounded": -0.55,
          "InflationRateFormatted": "-0.55",
          "Month": "/Date(962409600000)/",
          "MonthFormatted": "2000-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1373626373626373626373626374,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(959817600000)/",
          "MonthFormatted": "2000-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2754820936639118457300275482,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(957139200000)/",
          "MonthFormatted": "2000-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4149377593360995850622406639,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(954547200000)/",
          "MonthFormatted": "2000-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1385041551246537396121883657,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(951868800000)/",
          "MonthFormatted": "2000-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4172461752433936022253129346,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(949363200000)/",
          "MonthFormatted": "2000-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.9641873278236914600550964187,
          "InflationRateRounded": -0.96,
          "InflationRateFormatted": "-0.96",
          "Month": "/Date(946684800000)/",
          "MonthFormatted": "2000-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2762430939226519337016574586,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(944006400000)/",
          "MonthFormatted": "1999-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1383125864453665283540802213,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(941414400000)/",
          "MonthFormatted": "1999-11-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1381215469613259668508287293,
          "InflationRateRounded": -0.14,
          "InflationRateFormatted": "-0.14",
          "Month": "/Date(938736000000)/",
          "MonthFormatted": "1999-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4160887656033287101248266297,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(936144000000)/",
          "MonthFormatted": "1999-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2781641168289290681502086231,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(933465600000)/",
          "MonthFormatted": "1999-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5532503457814661134163208852,
          "InflationRateRounded": -0.55,
          "InflationRateFormatted": "-0.55",
          "Month": "/Date(930787200000)/",
          "MonthFormatted": "1999-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1381215469613259668508287293,
          "InflationRateRounded": -0.14,
          "InflationRateFormatted": "-0.14",
          "Month": "/Date(928195200000)/",
          "MonthFormatted": "1999-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2770083102493074792243767313,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(925516800000)/",
          "MonthFormatted": "1999-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4172461752433936022253129346,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(922924800000)/",
          "MonthFormatted": "1999-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5594405594405594405594405594,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(920246400000)/",
          "MonthFormatted": "1999-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1400560224089635854341736695,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(917827200000)/",
          "MonthFormatted": "1999-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5571030640668523676880222841,
          "InflationRateRounded": -0.56,
          "InflationRateFormatted": "-0.56",
          "Month": "/Date(915148800000)/",
          "MonthFormatted": "1999-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2793296089385474860335195531,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(912470400000)/",
          "MonthFormatted": "1998-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1398601398601398601398601399,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(909878400000)/",
          "MonthFormatted": "1998-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(907200000000)/",
          "MonthFormatted": "1998-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4213483146067415730337078652,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(904608000000)/",
          "MonthFormatted": "1998-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2816901408450704225352112676,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(901929600000)/",
          "MonthFormatted": "1998-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4207573632538569424964936886,
          "InflationRateRounded": -0.42,
          "InflationRateFormatted": "-0.42",
          "Month": "/Date(899251200000)/",
          "MonthFormatted": "1998-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1400560224089635854341736695,
          "InflationRateRounded": -0.14,
          "InflationRateFormatted": "-0.14",
          "Month": "/Date(896659200000)/",
          "MonthFormatted": "1998-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4219409282700421940928270042,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(893980800000)/",
          "MonthFormatted": "1998-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5657708628005657708628005658,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(891388800000)/",
          "MonthFormatted": "1998-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2836879432624113475177304965,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(888710400000)/",
          "MonthFormatted": "1998-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2844950213371266002844950213,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(886291200000)/",
          "MonthFormatted": "1998-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5657708628005657708628005658,
          "InflationRateRounded": -0.57,
          "InflationRateFormatted": "-0.57",
          "Month": "/Date(883612800000)/",
          "MonthFormatted": "1998-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.141643059490084985835694051,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(880934400000)/",
          "MonthFormatted": "1997-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(878342400000)/",
          "MonthFormatted": "1997-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(875664000000)/",
          "MonthFormatted": "1997-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.426742532005689900426742532,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(873072000000)/",
          "MonthFormatted": "1997-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5722460658082975679542203147,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(870393600000)/",
          "MonthFormatted": "1997-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4273504273504273504273504274,
          "InflationRateRounded": -0.43,
          "InflationRateFormatted": "-0.43",
          "Month": "/Date(867715200000)/",
          "MonthFormatted": "1997-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2857142857142857142857142857,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(865123200000)/",
          "MonthFormatted": "1997-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2865329512893982808022922636,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(862444800000)/",
          "MonthFormatted": "1997-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4316546762589928057553956835,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(859852800000)/",
          "MonthFormatted": "1997-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1440922190201729106628242075,
          "InflationRateRounded": 0.14,
          "InflationRateFormatted": "0.14",
          "Month": "/Date(857174400000)/",
          "MonthFormatted": "1997-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2890173410404624277456647399,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(854755200000)/",
          "MonthFormatted": "1997-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4316546762589928057553956835,
          "InflationRateRounded": -0.43,
          "InflationRateFormatted": "-0.43",
          "Month": "/Date(852076800000)/",
          "MonthFormatted": "1997-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2886002886002886002886002886,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(849398400000)/",
          "MonthFormatted": "1996-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(846806400000)/",
          "MonthFormatted": "1996-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(844128000000)/",
          "MonthFormatted": "1996-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5805515239477503628447024673,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(841536000000)/",
          "MonthFormatted": "1996-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4373177842565597667638483965,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(838857600000)/",
          "MonthFormatted": "1996-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.579710144927536231884057971,
          "InflationRateRounded": -0.58,
          "InflationRateFormatted": "-0.58",
          "Month": "/Date(836179200000)/",
          "MonthFormatted": "1996-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1451378809869375907111756168,
          "InflationRateRounded": 0.15,
          "InflationRateFormatted": "0.15",
          "Month": "/Date(833587200000)/",
          "MonthFormatted": "1996-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2911208151382823871906841339,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(830908800000)/",
          "MonthFormatted": "1996-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4385964912280701754385964912,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(828316800000)/",
          "MonthFormatted": "1996-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4405286343612334801762114537,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(825638400000)/",
          "MonthFormatted": "1996-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4424778761061946902654867257,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(823132800000)/",
          "MonthFormatted": "1996-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2941176470588235294117647059,
          "InflationRateRounded": -0.29,
          "InflationRateFormatted": "-0.29",
          "Month": "/Date(820454400000)/",
          "MonthFormatted": "1996-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5917159763313609467455621302,
          "InflationRateRounded": 0.59,
          "InflationRateFormatted": "0.59",
          "Month": "/Date(817776000000)/",
          "MonthFormatted": "1995-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(815184000000)/",
          "MonthFormatted": "1995-11-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1477104874446085672082717873,
          "InflationRateRounded": -0.15,
          "InflationRateFormatted": "-0.15",
          "Month": "/Date(812505600000)/",
          "MonthFormatted": "1995-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4451038575667655786350148368,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(809913600000)/",
          "MonthFormatted": "1995-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5970149253731343283582089552,
          "InflationRateRounded": 0.60,
          "InflationRateFormatted": "0.60",
          "Month": "/Date(807235200000)/",
          "MonthFormatted": "1995-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5934718100890207715133531157,
          "InflationRateRounded": -0.59,
          "InflationRateFormatted": "-0.59",
          "Month": "/Date(804556800000)/",
          "MonthFormatted": "1995-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(801964800000)/",
          "MonthFormatted": "1995-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5970149253731343283582089552,
          "InflationRateRounded": 0.60,
          "InflationRateFormatted": "0.60",
          "Month": "/Date(799286400000)/",
          "MonthFormatted": "1995-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6006006006006006006006006006,
          "InflationRateRounded": 0.60,
          "InflationRateFormatted": "0.60",
          "Month": "/Date(796694400000)/",
          "MonthFormatted": "1995-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4524886877828054298642533937,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(794016000000)/",
          "MonthFormatted": "1995-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4545454545454545454545454545,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(791596800000)/",
          "MonthFormatted": "1995-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(788918400000)/",
          "MonthFormatted": "1995-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4566210045662100456621004566,
          "InflationRateRounded": 0.46,
          "InflationRateFormatted": "0.46",
          "Month": "/Date(786240000000)/",
          "MonthFormatted": "1994-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1524390243902439024390243902,
          "InflationRateRounded": 0.15,
          "InflationRateFormatted": "0.15",
          "Month": "/Date(783648000000)/",
          "MonthFormatted": "1994-11-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3039513677811550151975683891,
          "InflationRateRounded": -0.30,
          "InflationRateFormatted": "-0.30",
          "Month": "/Date(780969600000)/",
          "MonthFormatted": "1994-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1522070015220700152207001522,
          "InflationRateRounded": 0.15,
          "InflationRateFormatted": "0.15",
          "Month": "/Date(778377600000)/",
          "MonthFormatted": "1994-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6125574272588055130168453292,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(775699200000)/",
          "MonthFormatted": "1994-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6088280060882800608828006088,
          "InflationRateRounded": -0.61,
          "InflationRateFormatted": "-0.61",
          "Month": "/Date(773020800000)/",
          "MonthFormatted": "1994-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(770428800000)/",
          "MonthFormatted": "1994-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3053435114503816793893129771,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(767750400000)/",
          "MonthFormatted": "1994-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.924499229583975346687211094,
          "InflationRateRounded": 0.92,
          "InflationRateFormatted": "0.92",
          "Month": "/Date(765158400000)/",
          "MonthFormatted": "1994-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3091190108191653786707882535,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(762480000000)/",
          "MonthFormatted": "1994-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4658385093167701863354037267,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(760060800000)/",
          "MonthFormatted": "1994-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4636785162287480680061823802,
          "InflationRateRounded": -0.46,
          "InflationRateFormatted": "-0.46",
          "Month": "/Date(757382400000)/",
          "MonthFormatted": "1994-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3100775193798449612403100775,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(754704000000)/",
          "MonthFormatted": "1993-12-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3091190108191653786707882535,
          "InflationRateRounded": -0.31,
          "InflationRateFormatted": "-0.31",
          "Month": "/Date(752112000000)/",
          "MonthFormatted": "1993-11-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1543209876543209876543209877,
          "InflationRateRounded": -0.15,
          "InflationRateFormatted": "-0.15",
          "Month": "/Date(749433600000)/",
          "MonthFormatted": "1993-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4651162790697674418604651163,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(746841600000)/",
          "MonthFormatted": "1993-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4672897196261682242990654206,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(744163200000)/",
          "MonthFormatted": "1993-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3105590062111801242236024845,
          "InflationRateRounded": -0.31,
          "InflationRateFormatted": "-0.31",
          "Month": "/Date(741484800000)/",
          "MonthFormatted": "1993-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1550387596899224806201550388,
          "InflationRateRounded": -0.16,
          "InflationRateFormatted": "-0.16",
          "Month": "/Date(738892800000)/",
          "MonthFormatted": "1993-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4672897196261682242990654206,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(736214400000)/",
          "MonthFormatted": "1993-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1023622047244094488188976378,
          "InflationRateRounded": 1.10,
          "InflationRateFormatted": "1.10",
          "Month": "/Date(733622400000)/",
          "MonthFormatted": "1993-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4746835443037974683544303797,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(730944000000)/",
          "MonthFormatted": "1993-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6369426751592356687898089172,
          "InflationRateRounded": 0.64,
          "InflationRateFormatted": "0.64",
          "Month": "/Date(728524800000)/",
          "MonthFormatted": "1993-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6329113924050632911392405063,
          "InflationRateRounded": -0.63,
          "InflationRateFormatted": "-0.63",
          "Month": "/Date(725846400000)/",
          "MonthFormatted": "1993-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1584786053882725832012678288,
          "InflationRateRounded": 0.16,
          "InflationRateFormatted": "0.16",
          "Month": "/Date(723168000000)/",
          "MonthFormatted": "1992-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(720576000000)/",
          "MonthFormatted": "1992-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3179650238473767885532591415,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(717897600000)/",
          "MonthFormatted": "1992-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.479233226837060702875399361,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(715305600000)/",
          "MonthFormatted": "1992-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(712627200000)/",
          "MonthFormatted": "1992-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4769475357710651828298887122,
          "InflationRateRounded": -0.48,
          "InflationRateFormatted": "-0.48",
          "Month": "/Date(709948800000)/",
          "MonthFormatted": "1992-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(707356800000)/",
          "MonthFormatted": "1992-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3189792663476874003189792663,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(704678400000)/",
          "MonthFormatted": "1992-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1290322580645161290322580645,
          "InflationRateRounded": 1.13,
          "InflationRateFormatted": "1.13",
          "Month": "/Date(702086400000)/",
          "MonthFormatted": "1992-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4862236628849270664505672609,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(699408000000)/",
          "MonthFormatted": "1992-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4885993485342019543973941368,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(696902400000)/",
          "MonthFormatted": "1992-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3246753246753246753246753247,
          "InflationRateRounded": -0.32,
          "InflationRateFormatted": "-0.32",
          "Month": "/Date(694224000000)/",
          "MonthFormatted": "1992-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1626016260162601626016260163,
          "InflationRateRounded": 0.16,
          "InflationRateFormatted": "0.16",
          "Month": "/Date(691545600000)/",
          "MonthFormatted": "1991-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3262642740619902120717781403,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(688953600000)/",
          "MonthFormatted": "1991-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4918032786885245901639344262,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(686275200000)/",
          "MonthFormatted": "1991-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4942339373970345963756177924,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(683683200000)/",
          "MonthFormatted": "1991-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4966887417218543046357615894,
          "InflationRateRounded": 0.50,
          "InflationRateFormatted": "0.50",
          "Month": "/Date(681004800000)/",
          "MonthFormatted": "1991-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.33003300330033003300330033,
          "InflationRateRounded": -0.33,
          "InflationRateFormatted": "-0.33",
          "Month": "/Date(678326400000)/",
          "MonthFormatted": "1991-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.497512437810945273631840796,
          "InflationRateRounded": 0.50,
          "InflationRateFormatted": "0.50",
          "Month": "/Date(675734400000)/",
          "MonthFormatted": "1991-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6677796327212020033388981636,
          "InflationRateRounded": 0.67,
          "InflationRateFormatted": "0.67",
          "Month": "/Date(673056000000)/",
          "MonthFormatted": "1991-05-01",
          "Country": 6
      },
      {
          "InflationRate": 3.4542314335060449050086355786,
          "InflationRateRounded": 3.45,
          "InflationRateFormatted": "3.45",
          "Month": "/Date(670464000000)/",
          "MonthFormatted": "1991-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.34662045060658578856152513,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(667785600000)/",
          "MonthFormatted": "1991-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5226480836236933797909407666,
          "InflationRateRounded": 0.52,
          "InflationRateFormatted": "0.52",
          "Month": "/Date(665366400000)/",
          "MonthFormatted": "1991-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(662688000000)/",
          "MonthFormatted": "1991-01-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1739130434782608695652173913,
          "InflationRateRounded": -0.17,
          "InflationRateFormatted": "-0.17",
          "Month": "/Date(660009600000)/",
          "MonthFormatted": "1990-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1742160278745644599303135889,
          "InflationRateRounded": 0.17,
          "InflationRateFormatted": "0.17",
          "Month": "/Date(657417600000)/",
          "MonthFormatted": "1990-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.701754385964912280701754386,
          "InflationRateRounded": 0.70,
          "InflationRateFormatted": "0.70",
          "Month": "/Date(654739200000)/",
          "MonthFormatted": "1990-10-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0638297872340425531914893617,
          "InflationRateRounded": 1.06,
          "InflationRateFormatted": "1.06",
          "Month": "/Date(652147200000)/",
          "MonthFormatted": "1990-09-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0752688172043010752688172043,
          "InflationRateRounded": 1.08,
          "InflationRateFormatted": "1.08",
          "Month": "/Date(649468800000)/",
          "MonthFormatted": "1990-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.178890876565295169946332737,
          "InflationRateRounded": -0.18,
          "InflationRateFormatted": "-0.18",
          "Month": "/Date(646790400000)/",
          "MonthFormatted": "1990-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3590664272890484739676840215,
          "InflationRateRounded": 0.36,
          "InflationRateFormatted": "0.36",
          "Month": "/Date(644198400000)/",
          "MonthFormatted": "1990-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9057971014492753623188405797,
          "InflationRateRounded": 0.91,
          "InflationRateFormatted": "0.91",
          "Month": "/Date(641520000000)/",
          "MonthFormatted": "1990-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.8450184501845018450184501845,
          "InflationRateRounded": 1.85,
          "InflationRateFormatted": "1.85",
          "Month": "/Date(638928000000)/",
          "MonthFormatted": "1990-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5565862708719851576994434137,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(636249600000)/",
          "MonthFormatted": "1990-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5597014925373134328358208955,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(633830400000)/",
          "MonthFormatted": "1990-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3745318352059925093632958801,
          "InflationRateRounded": 0.37,
          "InflationRateFormatted": "0.37",
          "Month": "/Date(631152000000)/",
          "MonthFormatted": "1990-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1876172607879924953095684803,
          "InflationRateRounded": 0.19,
          "InflationRateFormatted": "0.19",
          "Month": "/Date(628473600000)/",
          "MonthFormatted": "1989-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3766478342749529190207156309,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(625881600000)/",
          "MonthFormatted": "1989-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7590132827324478178368121442,
          "InflationRateRounded": 0.76,
          "InflationRateFormatted": "0.76",
          "Month": "/Date(623203200000)/",
          "MonthFormatted": "1989-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7648183556405353728489483748,
          "InflationRateRounded": 0.76,
          "InflationRateFormatted": "0.76",
          "Month": "/Date(620611200000)/",
          "MonthFormatted": "1989-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(617932800000)/",
          "MonthFormatted": "1989-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(615254400000)/",
          "MonthFormatted": "1989-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3838771593090211132437619962,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(612662400000)/",
          "MonthFormatted": "1989-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5791505791505791505791505792,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(609984000000)/",
          "MonthFormatted": "1989-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3698630136986301369863013699,
          "InflationRateRounded": 1.37,
          "InflationRateFormatted": "1.37",
          "Month": "/Date(607392000000)/",
          "MonthFormatted": "1989-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3929273084479371316306483301,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(604713600000)/",
          "MonthFormatted": "1989-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3944773175542406311637080868,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(602294400000)/",
          "MonthFormatted": "1989-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1976284584980237154150197628,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(599616000000)/",
          "MonthFormatted": "1989-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.198019801980198019801980198,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(596937600000)/",
          "MonthFormatted": "1988-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3976143141153081510934393638,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(594345600000)/",
          "MonthFormatted": "1988-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3992015968063872255489021956,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(591667200000)/",
          "MonthFormatted": "1988-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4008016032064128256513026052,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(589075200000)/",
          "MonthFormatted": "1988-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4024144869215291750503018109,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(586396800000)/",
          "MonthFormatted": "1988-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(583718400000)/",
          "MonthFormatted": "1988-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.404040404040404040404040404,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(581126400000)/",
          "MonthFormatted": "1988-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4056795131845841784989858012,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(578448000000)/",
          "MonthFormatted": "1988-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.2320328542094455852156057495,
          "InflationRateRounded": 1.23,
          "InflationRateFormatted": "1.23",
          "Month": "/Date(575856000000)/",
          "MonthFormatted": "1988-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4123711340206185567010309278,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(573177600000)/",
          "MonthFormatted": "1988-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2066115702479338842975206612,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(570672000000)/",
          "MonthFormatted": "1988-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(567993600000)/",
          "MonthFormatted": "1988-01-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0980632507967639127237067909,
          "InflationRateRounded": -0.10,
          "InflationRateFormatted": "-0.10",
          "Month": "/Date(565315200000)/",
          "MonthFormatted": "1987-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.492732200049273220004927322,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(562723200000)/",
          "MonthFormatted": "1987-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4702970297029702970297029703,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(560044800000)/",
          "MonthFormatted": "1987-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2979145978152929493545183714,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(557452800000)/",
          "MonthFormatted": "1987-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2988047808764940239043824701,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(554774400000)/",
          "MonthFormatted": "1987-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0995024875621890547263681592,
          "InflationRateRounded": -0.10,
          "InflationRateFormatted": "-0.10",
          "Month": "/Date(552096000000)/",
          "MonthFormatted": "1987-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(549504000000)/",
          "MonthFormatted": "1987-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.09960159362549800796812749,
          "InflationRateRounded": 0.10,
          "InflationRateFormatted": "0.10",
          "Month": "/Date(546825600000)/",
          "MonthFormatted": "1987-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1841773746535651297556059461,
          "InflationRateRounded": 1.18,
          "InflationRateFormatted": "1.18",
          "Month": "/Date(544233600000)/",
          "MonthFormatted": "1987-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2019691996970462004544306993,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(541555200000)/",
          "MonthFormatted": "1987-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4055766793409378960709759189,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(539136000000)/",
          "MonthFormatted": "1987-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3816793893129770992366412214,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(536457600000)/",
          "MonthFormatted": "1987-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3318866479448557569568547358,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(533779200000)/",
          "MonthFormatted": "1986-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8496395468589083419155509784,
          "InflationRateRounded": 0.85,
          "InflationRateFormatted": "0.85",
          "Month": "/Date(531187200000)/",
          "MonthFormatted": "1986-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1547189272821041774110366168,
          "InflationRateRounded": 0.15,
          "InflationRateFormatted": "0.15",
          "Month": "/Date(528508800000)/",
          "MonthFormatted": "1986-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4923555325213785954910598601,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(525916800000)/",
          "MonthFormatted": "1986-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3119313750974785547179620483,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(523238400000)/",
          "MonthFormatted": "1986-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.285121824779678589942975635,
          "InflationRateRounded": -0.29,
          "InflationRateFormatted": "-0.29",
          "Month": "/Date(520560000000)/",
          "MonthFormatted": "1986-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0518134715025906735751295337,
          "InflationRateRounded": -0.05,
          "InflationRateFormatted": "-0.05",
          "Month": "/Date(517968000000)/",
          "MonthFormatted": "1986-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1816766156241889436802491565,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(515289600000)/",
          "MonthFormatted": "1986-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9696016771488469601677148847,
          "InflationRateRounded": 0.97,
          "InflationRateFormatted": "0.97",
          "Month": "/Date(512697600000)/",
          "MonthFormatted": "1986-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1311991603253739176069273157,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(510019200000)/",
          "MonthFormatted": "1986-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3687121411640769028180142218,
          "InflationRateRounded": 0.37,
          "InflationRateFormatted": "0.37",
          "Month": "/Date(507600000000)/",
          "MonthFormatted": "1986-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2111375032990234890472420164,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(504921600000)/",
          "MonthFormatted": "1986-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1321353065539112050739957717,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(502243200000)/",
          "MonthFormatted": "1985-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3447361442588172898435428268,
          "InflationRateRounded": 0.34,
          "InflationRateFormatted": "0.34",
          "Month": "/Date(499651200000)/",
          "MonthFormatted": "1985-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1593625498007968127490039841,
          "InflationRateRounded": 0.16,
          "InflationRateFormatted": "0.16",
          "Month": "/Date(496972800000)/",
          "MonthFormatted": "1985-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0530926466684364215556145474,
          "InflationRateRounded": -0.05,
          "InflationRateFormatted": "-0.05",
          "Month": "/Date(494380800000)/",
          "MonthFormatted": "1985-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2661698163428267234495608198,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(491702400000)/",
          "MonthFormatted": "1985-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1859723698193411264612114772,
          "InflationRateRounded": -0.19,
          "InflationRateFormatted": "-0.19",
          "Month": "/Date(489024000000)/",
          "MonthFormatted": "1985-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2129925452609158679446219382,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(486432000000)/",
          "MonthFormatted": "1985-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4546670232682535437282695908,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(483753600000)/",
          "MonthFormatted": "1985-05-01",
          "Country": 6
      },
      {
          "InflationRate": 2.1305654192843485386506419011,
          "InflationRateRounded": 2.13,
          "InflationRateFormatted": "2.13",
          "Month": "/Date(481161600000)/",
          "MonthFormatted": "1985-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9374138406396470912599944858,
          "InflationRateRounded": 0.94,
          "InflationRateFormatted": "0.94",
          "Month": "/Date(478483200000)/",
          "MonthFormatted": "1985-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8060033351862145636464702613,
          "InflationRateRounded": 0.81,
          "InflationRateFormatted": "0.81",
          "Month": "/Date(476064000000)/",
          "MonthFormatted": "1985-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.362622036262203626220362622,
          "InflationRateRounded": 0.36,
          "InflationRateFormatted": "0.36",
          "Month": "/Date(473385600000)/",
          "MonthFormatted": "1985-01-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0836120401337792642140468227,
          "InflationRateRounded": -0.08,
          "InflationRateFormatted": "-0.08",
          "Month": "/Date(470707200000)/",
          "MonthFormatted": "1984-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3075202683813251327928431647,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(468115200000)/",
          "MonthFormatted": "1984-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6188466947960618846694796062,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(465436800000)/",
          "MonthFormatted": "1984-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1972942502818489289740698985,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(462844800000)/",
          "MonthFormatted": "1984-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9388335704125177809388335704,
          "InflationRateRounded": 0.94,
          "InflationRateFormatted": "0.94",
          "Month": "/Date(460166400000)/",
          "MonthFormatted": "1984-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1136686558681443591929525433,
          "InflationRateRounded": -0.11,
          "InflationRateFormatted": "-0.11",
          "Month": "/Date(457488000000)/",
          "MonthFormatted": "1984-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2564102564102564102564102564,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(454896000000)/",
          "MonthFormatted": "1984-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3717472118959107806691449814,
          "InflationRateRounded": 0.37,
          "InflationRateFormatted": "0.37",
          "Month": "/Date(452217600000)/",
          "MonthFormatted": "1984-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3329469718922051579252390611,
          "InflationRateRounded": 1.33,
          "InflationRateFormatted": "1.33",
          "Month": "/Date(449625600000)/",
          "MonthFormatted": "1984-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3197674418604651162790697674,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(446947200000)/",
          "MonthFormatted": "1984-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4086398131932282545242265032,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(444441600000)/",
          "MonthFormatted": "1984-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0583430571761960326721120187,
          "InflationRateRounded": -0.06,
          "InflationRateFormatted": "-0.06",
          "Month": "/Date(441763200000)/",
          "MonthFormatted": "1984-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2632348639953202690845276397,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(439084800000)/",
          "MonthFormatted": "1983-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3522160258291752274728500147,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(436492800000)/",
          "MonthFormatted": "1983-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3534609720176730486008836524,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(433814400000)/",
          "MonthFormatted": "1983-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4437869822485207100591715976,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(431222400000)/",
          "MonthFormatted": "1983-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4457652303120356612184249629,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(428544000000)/",
          "MonthFormatted": "1983-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5377950403346280250971018823,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(425865600000)/",
          "MonthFormatted": "1983-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2395926924228811021263851453,
          "InflationRateRounded": 0.24,
          "InflationRateFormatted": "0.24",
          "Month": "/Date(423273600000)/",
          "MonthFormatted": "1983-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4210526315789473684210526316,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(420595200000)/",
          "MonthFormatted": "1983-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4028667276608722171393717597,
          "InflationRateRounded": 1.40,
          "InflationRateFormatted": "1.40",
          "Month": "/Date(418003200000)/",
          "MonthFormatted": "1983-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1833180568285976168652612282,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(415324800000)/",
          "MonthFormatted": "1983-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4295796256520405032218471924,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(412905600000)/",
          "MonthFormatted": "1983-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1228878648233486943164362519,
          "InflationRateRounded": 0.12,
          "InflationRateFormatted": "0.12",
          "Month": "/Date(410227200000)/",
          "MonthFormatted": "1983-01-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1839926402943882244710211592,
          "InflationRateRounded": -0.18,
          "InflationRateFormatted": "-0.18",
          "Month": "/Date(407548800000)/",
          "MonthFormatted": "1982-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4930662557781201848998459168,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(404956800000)/",
          "MonthFormatted": "1982-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4955094456488076803964075565,
          "InflationRateRounded": 0.50,
          "InflationRateFormatted": "0.50",
          "Month": "/Date(402278400000)/",
          "MonthFormatted": "1982-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.0619003404518724852986691427,
          "InflationRateRounded": -0.06,
          "InflationRateFormatted": "-0.06",
          "Month": "/Date(399686400000)/",
          "MonthFormatted": "1982-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0309597523219814241486068111,
          "InflationRateRounded": 0.03,
          "InflationRateFormatted": "0.03",
          "Month": "/Date(397008000000)/",
          "MonthFormatted": "1982-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0309693403530504800247754723,
          "InflationRateRounded": 0.03,
          "InflationRateFormatted": "0.03",
          "Month": "/Date(394329600000)/",
          "MonthFormatted": "1982-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.279503105590062111801242236,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(391737600000)/",
          "MonthFormatted": "1982-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7194244604316546762589928058,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(389059200000)/",
          "MonthFormatted": "1982-05-01",
          "Country": 6
      },
      {
          "InflationRate": 2.0102105934907466496490108488,
          "InflationRateRounded": 2.01,
          "InflationRateFormatted": "2.01",
          "Month": "/Date(386467200000)/",
          "MonthFormatted": "1982-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8690054715159317669777920824,
          "InflationRateRounded": 0.87,
          "InflationRateFormatted": "0.87",
          "Month": "/Date(383788800000)/",
          "MonthFormatted": "1982-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.032195750160978750804893754,
          "InflationRateRounded": 0.03,
          "InflationRateFormatted": "0.03",
          "Month": "/Date(381369600000)/",
          "MonthFormatted": "1982-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5829015544041450777202072539,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(378691200000)/",
          "MonthFormatted": "1982-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6190941674812642554578038449,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(376012800000)/",
          "MonthFormatted": "1981-12-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0536713862364175172867961804,
          "InflationRateRounded": 1.05,
          "InflationRateFormatted": "1.05",
          "Month": "/Date(373420800000)/",
          "MonthFormatted": "1981-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8970099667774086378737541528,
          "InflationRateRounded": 0.90,
          "InflationRateFormatted": "0.90",
          "Month": "/Date(370742400000)/",
          "MonthFormatted": "1981-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5679919812896759104577347143,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(368150400000)/",
          "MonthFormatted": "1981-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7404914170313025917199596096,
          "InflationRateRounded": 0.74,
          "InflationRateFormatted": "0.74",
          "Month": "/Date(365472000000)/",
          "MonthFormatted": "1981-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.439486139283299526707234618,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(362793600000)/",
          "MonthFormatted": "1981-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5780346820809248554913294798,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(360201600000)/",
          "MonthFormatted": "1981-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6502395619438740588637919233,
          "InflationRateRounded": 0.65,
          "InflationRateFormatted": "0.65",
          "Month": "/Date(357523200000)/",
          "MonthFormatted": "1981-05-01",
          "Country": 6
      },
      {
          "InflationRate": 2.887323943661971830985915493,
          "InflationRateRounded": 2.89,
          "InflationRateFormatted": "2.89",
          "Month": "/Date(354931200000)/",
          "MonthFormatted": "1981-04-01",
          "Country": 6
      },
      {
          "InflationRate": 1.5010721944245889921372408863,
          "InflationRateRounded": 1.50,
          "InflationRateFormatted": "1.50",
          "Month": "/Date(352252800000)/",
          "MonthFormatted": "1981-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9015506671474936891453299675,
          "InflationRateRounded": 0.90,
          "InflationRateFormatted": "0.90",
          "Month": "/Date(349833600000)/",
          "MonthFormatted": "1981-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6168359941944847605224963716,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(347155200000)/",
          "MonthFormatted": "1981-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5472455308281649033199562204,
          "InflationRateRounded": 0.55,
          "InflationRateFormatted": "0.55",
          "Month": "/Date(344476800000)/",
          "MonthFormatted": "1980-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8091210003677822728944464877,
          "InflationRateRounded": 0.81,
          "InflationRateFormatted": "0.81",
          "Month": "/Date(341884800000)/",
          "MonthFormatted": "1980-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.629163582531458179126572909,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(339206400000)/",
          "MonthFormatted": "1980-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6331471135940409683426443203,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(336614400000)/",
          "MonthFormatted": "1980-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2239641657334826427771556551,
          "InflationRateRounded": 0.22,
          "InflationRateFormatted": "0.22",
          "Month": "/Date(333936000000)/",
          "MonthFormatted": "1980-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8280015054572826496048174633,
          "InflationRateRounded": 0.83,
          "InflationRateFormatted": "0.83",
          "Month": "/Date(331257600000)/",
          "MonthFormatted": "1980-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9498480243161094224924012158,
          "InflationRateRounded": 0.95,
          "InflationRateFormatted": "0.95",
          "Month": "/Date(328665600000)/",
          "MonthFormatted": "1980-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9202453987730061349693251534,
          "InflationRateRounded": 0.92,
          "InflationRateFormatted": "0.92",
          "Month": "/Date(325987200000)/",
          "MonthFormatted": "1980-05-01",
          "Country": 6
      },
      {
          "InflationRate": 3.4099920697858842188739095956,
          "InflationRateRounded": 3.41,
          "InflationRateFormatted": "3.41",
          "Month": "/Date(323395200000)/",
          "MonthFormatted": "1980-04-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3665594855305466237942122186,
          "InflationRateRounded": 1.37,
          "InflationRateFormatted": "1.37",
          "Month": "/Date(320716800000)/",
          "MonthFormatted": "1980-03-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4268242967794537301263758663,
          "InflationRateRounded": 1.43,
          "InflationRateFormatted": "1.43",
          "Month": "/Date(318211200000)/",
          "MonthFormatted": "1980-02-01",
          "Country": 6
      },
      {
          "InflationRate": 2.4644945697577276524644945698,
          "InflationRateRounded": 2.46,
          "InflationRateFormatted": "2.46",
          "Month": "/Date(315532800000)/",
          "MonthFormatted": "1980-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7151872107698779974758098443,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(312854400000)/",
          "MonthFormatted": "1979-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8913412563667232597623089983,
          "InflationRateRounded": 0.89,
          "InflationRateFormatted": "0.89",
          "Month": "/Date(310262400000)/",
          "MonthFormatted": "1979-11-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0291595197255574614065180103,
          "InflationRateRounded": 1.03,
          "InflationRateFormatted": "1.03",
          "Month": "/Date(307584000000)/",
          "MonthFormatted": "1979-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9961022087483759203118233001,
          "InflationRateRounded": 1.00,
          "InflationRateFormatted": "1.00",
          "Month": "/Date(304992000000)/",
          "MonthFormatted": "1979-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7856831078131820165866433872,
          "InflationRateRounded": 0.79,
          "InflationRateFormatted": "0.79",
          "Month": "/Date(302313600000)/",
          "MonthFormatted": "1979-08-01",
          "Country": 6
      },
      {
          "InflationRate": 4.32604735883424408014571949,
          "InflationRateRounded": 4.33,
          "InflationRateFormatted": "4.33",
          "Month": "/Date(299635200000)/",
          "MonthFormatted": "1979-07-01",
          "Country": 6
      },
      {
          "InflationRate": 1.7137563686892079666512274201,
          "InflationRateRounded": 1.71,
          "InflationRateFormatted": "1.71",
          "Month": "/Date(297043200000)/",
          "MonthFormatted": "1979-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7936507936507936507936507937,
          "InflationRateRounded": 0.79,
          "InflationRateFormatted": "0.79",
          "Month": "/Date(294364800000)/",
          "MonthFormatted": "1979-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.7094017094017094017094017094,
          "InflationRateRounded": 1.71,
          "InflationRateFormatted": "1.71",
          "Month": "/Date(291772800000)/",
          "MonthFormatted": "1979-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8137865007180469123982766874,
          "InflationRateRounded": 0.81,
          "InflationRateFormatted": "0.81",
          "Month": "/Date(289094400000)/",
          "MonthFormatted": "1979-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8204633204633204633204633205,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(286675200000)/",
          "MonthFormatted": "1979-02-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4691478942213516160626836435,
          "InflationRateRounded": 1.47,
          "InflationRateFormatted": "1.47",
          "Month": "/Date(283996800000)/",
          "MonthFormatted": "1979-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8395061728395061728395061728,
          "InflationRateRounded": 0.84,
          "InflationRateFormatted": "0.84",
          "Month": "/Date(281318400000)/",
          "MonthFormatted": "1978-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6961710591745400298359025361,
          "InflationRateRounded": 0.70,
          "InflationRateFormatted": "0.70",
          "Month": "/Date(278726400000)/",
          "MonthFormatted": "1978-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4495504495504495504495504496,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(276048000000)/",
          "MonthFormatted": "1978-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4012036108324974924774322969,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(273456000000)/",
          "MonthFormatted": "1978-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6562342251388187783947501262,
          "InflationRateRounded": 0.66,
          "InflationRateFormatted": "0.66",
          "Month": "/Date(270777600000)/",
          "MonthFormatted": "1978-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4563894523326572008113590264,
          "InflationRateRounded": 0.46,
          "InflationRateFormatted": "0.46",
          "Month": "/Date(268099200000)/",
          "MonthFormatted": "1978-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7664793050587634133878385284,
          "InflationRateRounded": 0.77,
          "InflationRateFormatted": "0.77",
          "Month": "/Date(265507200000)/",
          "MonthFormatted": "1978-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5652620760534429599177800617,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(262828800000)/",
          "MonthFormatted": "1978-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4598540145985401459854014599,
          "InflationRateRounded": 1.46,
          "InflationRateFormatted": "1.46",
          "Month": "/Date(260236800000)/",
          "MonthFormatted": "1978-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6295907660020986358866736621,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(257558400000)/",
          "MonthFormatted": "1978-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5804749340369393139841688654,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(255139200000)/",
          "MonthFormatted": "1978-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5838641188959660297239915074,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(252460800000)/",
          "MonthFormatted": "1978-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5336179295624332977588046958,
          "InflationRateRounded": 0.53,
          "InflationRateFormatted": "0.53",
          "Month": "/Date(249782400000)/",
          "MonthFormatted": "1977-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4825737265415549597855227882,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(247190400000)/",
          "MonthFormatted": "1977-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4308023694130317716747442111,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(244512000000)/",
          "MonthFormatted": "1977-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5414185165132647536545749865,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(241920000000)/",
          "MonthFormatted": "1977-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4896626768226332970620239391,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(239241600000)/",
          "MonthFormatted": "1977-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1089324618736383442265795207,
          "InflationRateRounded": 0.11,
          "InflationRateFormatted": "0.11",
          "Month": "/Date(236563200000)/",
          "MonthFormatted": "1977-07-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0456796917996697853604843148,
          "InflationRateRounded": 1.05,
          "InflationRateFormatted": "1.05",
          "Month": "/Date(233971200000)/",
          "MonthFormatted": "1977-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7764836383804769828064337216,
          "InflationRateRounded": 0.78,
          "InflationRateFormatted": "0.78",
          "Month": "/Date(231292800000)/",
          "MonthFormatted": "1977-05-01",
          "Country": 6
      },
      {
          "InflationRate": 2.5597269624573378839590443686,
          "InflationRateRounded": 2.56,
          "InflationRateFormatted": "2.56",
          "Month": "/Date(228700800000)/",
          "MonthFormatted": "1977-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9764503159103963239517518667,
          "InflationRateRounded": 0.98,
          "InflationRateFormatted": "0.98",
          "Month": "/Date(226022400000)/",
          "MonthFormatted": "1977-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9860788863109048723897911833,
          "InflationRateRounded": 0.99,
          "InflationRateFormatted": "0.99",
          "Month": "/Date(223603200000)/",
          "MonthFormatted": "1977-02-01",
          "Country": 6
      },
      {
          "InflationRate": 2.619047619047619047619047619,
          "InflationRateRounded": 2.62,
          "InflationRateFormatted": "2.62",
          "Month": "/Date(220924800000)/",
          "MonthFormatted": "1977-01-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3268998793727382388419782871,
          "InflationRateRounded": 1.33,
          "InflationRateFormatted": "1.33",
          "Month": "/Date(218246400000)/",
          "MonthFormatted": "1976-12-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4067278287461773700305810398,
          "InflationRateRounded": 1.41,
          "InflationRateFormatted": "1.41",
          "Month": "/Date(215654400000)/",
          "MonthFormatted": "1976-11-01",
          "Country": 6
      },
      {
          "InflationRate": 1.8057285180572851805728518057,
          "InflationRateRounded": 1.81,
          "InflationRateFormatted": "1.81",
          "Month": "/Date(212976000000)/",
          "MonthFormatted": "1976-10-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3249211356466876971608832808,
          "InflationRateRounded": 1.32,
          "InflationRateFormatted": "1.32",
          "Month": "/Date(210384000000)/",
          "MonthFormatted": "1976-09-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4075495841330774152271273193,
          "InflationRateRounded": 1.41,
          "InflationRateFormatted": "1.41",
          "Month": "/Date(207705600000)/",
          "MonthFormatted": "1976-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1923076923076923076923076923,
          "InflationRateRounded": 0.19,
          "InflationRateFormatted": "0.19",
          "Month": "/Date(205027200000)/",
          "MonthFormatted": "1976-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5154639175257731958762886598,
          "InflationRateRounded": 0.52,
          "InflationRateFormatted": "0.52",
          "Month": "/Date(202435200000)/",
          "MonthFormatted": "1976-06-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1074918566775244299674267101,
          "InflationRateRounded": 1.11,
          "InflationRateFormatted": "1.11",
          "Month": "/Date(199756800000)/",
          "MonthFormatted": "1976-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.9256308100929614873837981408,
          "InflationRateRounded": 1.93,
          "InflationRateFormatted": "1.93",
          "Month": "/Date(197164800000)/",
          "MonthFormatted": "1976-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5340453938584779706275033378,
          "InflationRateRounded": 0.53,
          "InflationRateFormatted": "0.53",
          "Month": "/Date(194486400000)/",
          "MonthFormatted": "1976-03-01",
          "Country": 6
      },
      {
          "InflationRate": 1.2846517917511832319134550372,
          "InflationRateRounded": 1.28,
          "InflationRateFormatted": "1.28",
          "Month": "/Date(191980800000)/",
          "MonthFormatted": "1976-02-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3013698630136986301369863014,
          "InflationRateRounded": 1.30,
          "InflationRateFormatted": "1.30",
          "Month": "/Date(189302400000)/",
          "MonthFormatted": "1976-01-01",
          "Country": 6
      },
      {
          "InflationRate": 1.248266296809986130374479889,
          "InflationRateRounded": 1.25,
          "InflationRateFormatted": "1.25",
          "Month": "/Date(186624000000)/",
          "MonthFormatted": "1975-12-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1929824561403508771929824561,
          "InflationRateRounded": 1.19,
          "InflationRateFormatted": "1.19",
          "Month": "/Date(184032000000)/",
          "MonthFormatted": "1975-11-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4234875444839857651245551601,
          "InflationRateRounded": 1.42,
          "InflationRateFormatted": "1.42",
          "Month": "/Date(181353600000)/",
          "MonthFormatted": "1975-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8614501076812634601579325197,
          "InflationRateRounded": 0.86,
          "InflationRateFormatted": "0.86",
          "Month": "/Date(178761600000)/",
          "MonthFormatted": "1975-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5776173285198555956678700361,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(176083200000)/",
          "MonthFormatted": "1975-08-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0211524434719183078045222465,
          "InflationRateRounded": 1.02,
          "InflationRateFormatted": "1.02",
          "Month": "/Date(173404800000)/",
          "MonthFormatted": "1975-07-01",
          "Country": 6
      },
      {
          "InflationRate": 1.9330855018587360594795539033,
          "InflationRateRounded": 1.93,
          "InflationRateFormatted": "1.93",
          "Month": "/Date(170812800000)/",
          "MonthFormatted": "1975-06-01",
          "Country": 6
      },
      {
          "InflationRate": 4.1828040278853601859024012393,
          "InflationRateRounded": 4.18,
          "InflationRateFormatted": "4.18",
          "Month": "/Date(168134400000)/",
          "MonthFormatted": "1975-05-01",
          "Country": 6
      },
      {
          "InflationRate": 3.8616251005631536604987932422,
          "InflationRateRounded": 3.86,
          "InflationRateFormatted": "3.86",
          "Month": "/Date(165542400000)/",
          "MonthFormatted": "1975-04-01",
          "Country": 6
      },
      {
          "InflationRate": 1.968826907301066447908121411,
          "InflationRateRounded": 1.97,
          "InflationRateFormatted": "1.97",
          "Month": "/Date(162864000000)/",
          "MonthFormatted": "1975-03-01",
          "Country": 6
      },
      {
          "InflationRate": 1.6680567139282735613010842369,
          "InflationRateRounded": 1.67,
          "InflationRateFormatted": "1.67",
          "Month": "/Date(160444800000)/",
          "MonthFormatted": "1975-02-01",
          "Country": 6
      },
      {
          "InflationRate": 2.5662959794696321642429426861,
          "InflationRateRounded": 2.57,
          "InflationRateFormatted": "2.57",
          "Month": "/Date(157766400000)/",
          "MonthFormatted": "1975-01-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4756944444444444444444444444,
          "InflationRateRounded": 1.48,
          "InflationRateFormatted": "1.48",
          "Month": "/Date(155088000000)/",
          "MonthFormatted": "1974-12-01",
          "Country": 6
      },
      {
          "InflationRate": 1.7667844522968197879858657244,
          "InflationRateRounded": 1.77,
          "InflationRateFormatted": "1.77",
          "Month": "/Date(152496000000)/",
          "MonthFormatted": "1974-11-01",
          "Country": 6
      },
      {
          "InflationRate": 1.981981981981981981981981982,
          "InflationRateRounded": 1.98,
          "InflationRateFormatted": "1.98",
          "Month": "/Date(149817600000)/",
          "MonthFormatted": "1974-10-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0928961748633879781420765027,
          "InflationRateRounded": 1.09,
          "InflationRateFormatted": "1.09",
          "Month": "/Date(147225600000)/",
          "MonthFormatted": "1974-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.0911577028258887876025524157,
          "InflationRateRounded": 0.09,
          "InflationRateFormatted": "0.09",
          "Month": "/Date(144547200000)/",
          "MonthFormatted": "1974-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9199632014719411223551057958,
          "InflationRateRounded": 0.92,
          "InflationRateFormatted": "0.92",
          "Month": "/Date(141868800000)/",
          "MonthFormatted": "1974-07-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0223048327137546468401486989,
          "InflationRateRounded": 1.02,
          "InflationRateFormatted": "1.02",
          "Month": "/Date(139276800000)/",
          "MonthFormatted": "1974-06-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4137606032045240339302544769,
          "InflationRateRounded": 1.41,
          "InflationRateFormatted": "1.41",
          "Month": "/Date(136598400000)/",
          "MonthFormatted": "1974-05-01",
          "Country": 6
      },
      {
          "InflationRate": 3.4113060428849902534113060429,
          "InflationRateRounded": 3.41,
          "InflationRateFormatted": "3.41",
          "Month": "/Date(134006400000)/",
          "MonthFormatted": "1974-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8849557522123893805309734513,
          "InflationRateRounded": 0.88,
          "InflationRateFormatted": "0.88",
          "Month": "/Date(131328000000)/",
          "MonthFormatted": "1974-03-01",
          "Country": 6
      },
      {
          "InflationRate": 1.7,
          "InflationRateRounded": 1.7,
          "InflationRateFormatted": "1.70",
          "Month": "/Date(128908800000)/",
          "MonthFormatted": "1974-02-01",
          "Country": 6
      },
      {
          "InflationRate": 1.9367991845056065239551478084,
          "InflationRateRounded": 1.94,
          "InflationRateFormatted": "1.94",
          "Month": "/Date(126230400000)/",
          "MonthFormatted": "1974-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7186858316221765913757700205,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(123552000000)/",
          "MonthFormatted": "1973-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7238883143743536711478800414,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(120960000000)/",
          "MonthFormatted": "1973-11-01",
          "Country": 6
      },
      {
          "InflationRate": 2.00421940928270042194092827,
          "InflationRateRounded": 2.00,
          "InflationRateFormatted": "2.00",
          "Month": "/Date(118281600000)/",
          "MonthFormatted": "1973-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8510638297872340425531914894,
          "InflationRateRounded": 0.85,
          "InflationRateFormatted": "0.85",
          "Month": "/Date(115689600000)/",
          "MonthFormatted": "1973-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3201707577374599786552828175,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(113011200000)/",
          "MonthFormatted": "1973-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4287245444801714898177920686,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(110332800000)/",
          "MonthFormatted": "1973-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5387931034482758620689655172,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(107740800000)/",
          "MonthFormatted": "1973-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7600434310532030401737242128,
          "InflationRateRounded": 0.76,
          "InflationRateFormatted": "0.76",
          "Month": "/Date(105062400000)/",
          "MonthFormatted": "1973-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.8805309734513274336283185841,
          "InflationRateRounded": 1.88,
          "InflationRateFormatted": "1.88",
          "Month": "/Date(102470400000)/",
          "MonthFormatted": "1973-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5561735261401557285873192436,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(99792000000)/",
          "MonthFormatted": "1973-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6718924972004479283314669653,
          "InflationRateRounded": 0.67,
          "InflationRateFormatted": "0.67",
          "Month": "/Date(97372800000)/",
          "MonthFormatted": "1973-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6764374295377677564825253664,
          "InflationRateRounded": 0.68,
          "InflationRateFormatted": "0.68",
          "Month": "/Date(94694400000)/",
          "MonthFormatted": "1973-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4530011325028312570781426954,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(92016000000)/",
          "MonthFormatted": "1972-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3409090909090909090909090909,
          "InflationRateRounded": 0.34,
          "InflationRateFormatted": "0.34",
          "Month": "/Date(89424000000)/",
          "MonthFormatted": "1972-11-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3824884792626728110599078341,
          "InflationRateRounded": 1.38,
          "InflationRateFormatted": "1.38",
          "Month": "/Date(86745600000)/",
          "MonthFormatted": "1972-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5793742757821552723059096176,
          "InflationRateRounded": 0.58,
          "InflationRateFormatted": "0.58",
          "Month": "/Date(84153600000)/",
          "MonthFormatted": "1972-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.817757009345794392523364486,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(81475200000)/",
          "MonthFormatted": "1972-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3516998827667057444314185229,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(78796800000)/",
          "MonthFormatted": "1972-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5896226415094339622641509434,
          "InflationRateRounded": 0.59,
          "InflationRateFormatted": "0.59",
          "Month": "/Date(76204800000)/",
          "MonthFormatted": "1972-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4739336492890995260663507109,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(73526400000)/",
          "MonthFormatted": "1972-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.956937799043062200956937799,
          "InflationRateRounded": 0.96,
          "InflationRateFormatted": "0.96",
          "Month": "/Date(70934400000)/",
          "MonthFormatted": "1972-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3601440576230492196878751501,
          "InflationRateRounded": 0.36,
          "InflationRateFormatted": "0.36",
          "Month": "/Date(68256000000)/",
          "MonthFormatted": "1972-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.482509047044632086851628468,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(65750400000)/",
          "MonthFormatted": "1972-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.606796116504854368932038835,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(63072000000)/",
          "MonthFormatted": "1972-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4878048780487804878048780488,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(60393600000)/",
          "MonthFormatted": "1971-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6134969325153374233128834356,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(57801600000)/",
          "MonthFormatted": "1971-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4932182490752157829839704069,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(55123200000)/",
          "MonthFormatted": "1971-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1234567901234567901234567901,
          "InflationRateRounded": 0.12,
          "InflationRateFormatted": "0.12",
          "Month": "/Date(52531200000)/",
          "MonthFormatted": "1971-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1236093943139678615574783684,
          "InflationRateRounded": 0.12,
          "InflationRateFormatted": "0.12",
          "Month": "/Date(49852800000)/",
          "MonthFormatted": "1971-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.621890547263681592039800995,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(47174400000)/",
          "MonthFormatted": "1971-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6257822277847309136420525657,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(44582400000)/",
          "MonthFormatted": "1971-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6297229219143576826196473552,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(41904000000)/",
          "MonthFormatted": "1971-05-01",
          "Country": 6
      },
      {
          "InflationRate": 2.1879021879021879021879021879,
          "InflationRateRounded": 2.19,
          "InflationRateFormatted": "2.19",
          "Month": "/Date(39312000000)/",
          "MonthFormatted": "1971-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7782101167315175097276264591,
          "InflationRateRounded": 0.78,
          "InflationRateFormatted": "0.78",
          "Month": "/Date(36633600000)/",
          "MonthFormatted": "1971-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6527415143603133159268929504,
          "InflationRateRounded": 0.65,
          "InflationRateFormatted": "0.65",
          "Month": "/Date(34214400000)/",
          "MonthFormatted": "1971-02-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3227513227513227513227513228,
          "InflationRateRounded": 1.32,
          "InflationRateFormatted": "1.32",
          "Month": "/Date(31536000000)/",
          "MonthFormatted": "1971-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6657789613848202396804260985,
          "InflationRateRounded": 0.67,
          "InflationRateFormatted": "0.67",
          "Month": "/Date(28857600000)/",
          "MonthFormatted": "1970-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6702412868632707774798927614,
          "InflationRateRounded": 0.67,
          "InflationRateFormatted": "0.67",
          "Month": "/Date(26265600000)/",
          "MonthFormatted": "1970-11-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0840108401084010840108401084,
          "InflationRateRounded": 1.08,
          "InflationRateFormatted": "1.08",
          "Month": "/Date(23587200000)/",
          "MonthFormatted": "1970-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5449591280653950953678474114,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(20995200000)/",
          "MonthFormatted": "1970-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1360544217687074829931972789,
          "InflationRateRounded": -0.14,
          "InflationRateFormatted": "-0.14",
          "Month": "/Date(18316800000)/",
          "MonthFormatted": "1970-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8230452674897119341563786008,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(15638400000)/",
          "MonthFormatted": "1970-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2751031636863823933975240715,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(13046400000)/",
          "MonthFormatted": "1970-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2758620689655172413793103448,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(10368000000)/",
          "MonthFormatted": "1970-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.5406162464985994397759103641,
          "InflationRateRounded": 1.54,
          "InflationRateFormatted": "1.54",
          "Month": "/Date(7776000000)/",
          "MonthFormatted": "1970-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5633802816901408450704225352,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(5097600000)/",
          "MonthFormatted": "1970-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.566572237960339943342776204,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(2678400000)/",
          "MonthFormatted": "1970-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7132667617689015691868758916,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(0)/",
          "MonthFormatted": "1970-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7183908045977011494252873563,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(-2678400000)/",
          "MonthFormatted": "1969-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.288184438040345821325648415,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(-5270400000)/",
          "MonthFormatted": "1969-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7256894049346879535558780842,
          "InflationRateRounded": 0.73,
          "InflationRateFormatted": "0.73",
          "Month": "/Date(-7948800000)/",
          "MonthFormatted": "1969-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2911208151382823871906841339,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(-10540800000)/",
          "MonthFormatted": "1969-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2902757619738751814223512337,
          "InflationRateRounded": -0.29,
          "InflationRateFormatted": "-0.29",
          "Month": "/Date(-13219200000)/",
          "MonthFormatted": "1969-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-15897600000)/",
          "MonthFormatted": "1969-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4373177842565597667638483965,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(-18489600000)/",
          "MonthFormatted": "1969-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.145560407569141193595342067,
          "InflationRateRounded": -0.15,
          "InflationRateFormatted": "-0.15",
          "Month": "/Date(-21168000000)/",
          "MonthFormatted": "1969-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1782032400589101620029455081,
          "InflationRateRounded": 1.18,
          "InflationRateFormatted": "1.18",
          "Month": "/Date(-23760000000)/",
          "MonthFormatted": "1969-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2954209748892171344165435746,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(-26438400000)/",
          "MonthFormatted": "1969-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5943536404160475482912332838,
          "InflationRateRounded": 0.59,
          "InflationRateFormatted": "0.59",
          "Month": "/Date(-28857600000)/",
          "MonthFormatted": "1969-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5979073243647234678624813154,
          "InflationRateRounded": 0.60,
          "InflationRateFormatted": "0.60",
          "Month": "/Date(-31536000000)/",
          "MonthFormatted": "1969-01-01",
          "Country": 6
      },
      {
          "InflationRate": 1.2102874432677760968229954614,
          "InflationRateRounded": 1.21,
          "InflationRateFormatted": "1.21",
          "Month": "/Date(-34214400000)/",
          "MonthFormatted": "1968-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3034901365705614567526555387,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(-36806400000)/",
          "MonthFormatted": "1968-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4573170731707317073170731707,
          "InflationRateRounded": 0.46,
          "InflationRateFormatted": "0.46",
          "Month": "/Date(-39484800000)/",
          "MonthFormatted": "1968-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1526717557251908396946564885,
          "InflationRateRounded": 0.15,
          "InflationRateFormatted": "0.15",
          "Month": "/Date(-42076800000)/",
          "MonthFormatted": "1968-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1529051987767584097859327217,
          "InflationRateRounded": 0.15,
          "InflationRateFormatted": "0.15",
          "Month": "/Date(-44755200000)/",
          "MonthFormatted": "1968-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-47433600000)/",
          "MonthFormatted": "1968-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4608294930875576036866359447,
          "InflationRateRounded": 0.46,
          "InflationRateFormatted": "0.46",
          "Month": "/Date(-50025600000)/",
          "MonthFormatted": "1968-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-52704000000)/",
          "MonthFormatted": "1968-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.8779342723004694835680751174,
          "InflationRateRounded": 1.88,
          "InflationRateFormatted": "1.88",
          "Month": "/Date(-55296000000)/",
          "MonthFormatted": "1968-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3139717425431711145996860283,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(-57974400000)/",
          "MonthFormatted": "1968-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4731861198738170347003154574,
          "InflationRateRounded": 0.47,
          "InflationRateFormatted": "0.47",
          "Month": "/Date(-60480000000)/",
          "MonthFormatted": "1968-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3164556962025316455696202532,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(-63158400000)/",
          "MonthFormatted": "1968-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6369426751592356687898089172,
          "InflationRateRounded": 0.64,
          "InflationRateFormatted": "0.64",
          "Month": "/Date(-65836800000)/",
          "MonthFormatted": "1967-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.641025641025641025641025641,
          "InflationRateRounded": 0.64,
          "InflationRateFormatted": "0.64",
          "Month": "/Date(-68428800000)/",
          "MonthFormatted": "1967-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8077544426494345718901453958,
          "InflationRateRounded": 0.81,
          "InflationRateFormatted": "0.81",
          "Month": "/Date(-71107200000)/",
          "MonthFormatted": "1967-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1612903225806451612903225806,
          "InflationRateRounded": -0.16,
          "InflationRateFormatted": "-0.16",
          "Month": "/Date(-73699200000)/",
          "MonthFormatted": "1967-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1610305958132045088566827697,
          "InflationRateRounded": -0.16,
          "InflationRateFormatted": "-0.16",
          "Month": "/Date(-76377600000)/",
          "MonthFormatted": "1967-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.64,
          "InflationRateRounded": -0.64,
          "InflationRateFormatted": "-0.64",
          "Month": "/Date(-79056000000)/",
          "MonthFormatted": "1967-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3210272873194221508828250401,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(-81648000000)/",
          "MonthFormatted": "1967-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-84326400000)/",
          "MonthFormatted": "1967-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8090614886731391585760517799,
          "InflationRateRounded": 0.81,
          "InflationRateFormatted": "0.81",
          "Month": "/Date(-86918400000)/",
          "MonthFormatted": "1967-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-89596800000)/",
          "MonthFormatted": "1967-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-92016000000)/",
          "MonthFormatted": "1967-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1620745542949756888168557536,
          "InflationRateRounded": 0.16,
          "InflationRateFormatted": "0.16",
          "Month": "/Date(-94694400000)/",
          "MonthFormatted": "1967-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1623376623376623376623376623,
          "InflationRateRounded": 0.16,
          "InflationRateFormatted": "0.16",
          "Month": "/Date(-97372800000)/",
          "MonthFormatted": "1966-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6535947712418300653594771242,
          "InflationRateRounded": 0.65,
          "InflationRateFormatted": "0.65",
          "Month": "/Date(-99964800000)/",
          "MonthFormatted": "1966-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1636661211129296235679214403,
          "InflationRateRounded": 0.16,
          "InflationRateFormatted": "0.16",
          "Month": "/Date(-102643200000)/",
          "MonthFormatted": "1966-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.163398692810457516339869281,
          "InflationRateRounded": -0.16,
          "InflationRateFormatted": "-0.16",
          "Month": "/Date(-105235200000)/",
          "MonthFormatted": "1966-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6578947368421052631578947368,
          "InflationRateRounded": 0.66,
          "InflationRateFormatted": "0.66",
          "Month": "/Date(-107913600000)/",
          "MonthFormatted": "1966-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4909983633387888707037643208,
          "InflationRateRounded": -0.49,
          "InflationRateFormatted": "-0.49",
          "Month": "/Date(-110592000000)/",
          "MonthFormatted": "1966-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3284072249589490968801313629,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(-113184000000)/",
          "MonthFormatted": "1966-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6611570247933884297520661157,
          "InflationRateRounded": 0.66,
          "InflationRateFormatted": "0.66",
          "Month": "/Date(-115862400000)/",
          "MonthFormatted": "1966-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3400335008375209380234505863,
          "InflationRateRounded": 1.34,
          "InflationRateFormatted": "1.34",
          "Month": "/Date(-118454400000)/",
          "MonthFormatted": "1966-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1677852348993288590604026846,
          "InflationRateRounded": 0.17,
          "InflationRateFormatted": "0.17",
          "Month": "/Date(-121132800000)/",
          "MonthFormatted": "1966-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-123552000000)/",
          "MonthFormatted": "1966-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1680672268907563025210084034,
          "InflationRateRounded": 0.17,
          "InflationRateFormatted": "0.17",
          "Month": "/Date(-126230400000)/",
          "MonthFormatted": "1966-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5067567567567567567567567568,
          "InflationRateRounded": 0.51,
          "InflationRateFormatted": "0.51",
          "Month": "/Date(-128908800000)/",
          "MonthFormatted": "1965-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3389830508474576271186440678,
          "InflationRateRounded": 0.34,
          "InflationRateFormatted": "0.34",
          "Month": "/Date(-131500800000)/",
          "MonthFormatted": "1965-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1697792869269949066213921902,
          "InflationRateRounded": 0.17,
          "InflationRateFormatted": "0.17",
          "Month": "/Date(-134179200000)/",
          "MonthFormatted": "1965-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-136771200000)/",
          "MonthFormatted": "1965-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1700680272108843537414965986,
          "InflationRateRounded": 0.17,
          "InflationRateFormatted": "0.17",
          "Month": "/Date(-139449600000)/",
          "MonthFormatted": "1965-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-142128000000)/",
          "MonthFormatted": "1965-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3412969283276450511945392491,
          "InflationRateRounded": 0.34,
          "InflationRateFormatted": "0.34",
          "Month": "/Date(-144720000000)/",
          "MonthFormatted": "1965-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3424657534246575342465753425,
          "InflationRateRounded": 0.34,
          "InflationRateFormatted": "0.34",
          "Month": "/Date(-147398400000)/",
          "MonthFormatted": "1965-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.9197207678883071553228621291,
          "InflationRateRounded": 1.92,
          "InflationRateFormatted": "1.92",
          "Month": "/Date(-149990400000)/",
          "MonthFormatted": "1965-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3502626970227670753064798599,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(-152668800000)/",
          "MonthFormatted": "1965-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-155088000000)/",
          "MonthFormatted": "1965-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3514938488576449912126537786,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(-157766400000)/",
          "MonthFormatted": "1965-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3527336860670194003527336861,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(-160444800000)/",
          "MonthFormatted": "1964-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7104795737122557726465364121,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(-163036800000)/",
          "MonthFormatted": "1964-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.177935943060498220640569395,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(-165715200000)/",
          "MonthFormatted": "1964-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-168307200000)/",
          "MonthFormatted": "1964-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3571428571428571428571428571,
          "InflationRateRounded": 0.36,
          "InflationRateFormatted": "0.36",
          "Month": "/Date(-170985600000)/",
          "MonthFormatted": "1964-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-173664000000)/",
          "MonthFormatted": "1964-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3584229390681003584229390681,
          "InflationRateRounded": 0.36,
          "InflationRateFormatted": "0.36",
          "Month": "/Date(-176256000000)/",
          "MonthFormatted": "1964-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9041591320072332730560578662,
          "InflationRateRounded": 0.90,
          "InflationRateFormatted": "0.90",
          "Month": "/Date(-178934400000)/",
          "MonthFormatted": "1964-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9124087591240875912408759124,
          "InflationRateRounded": 0.91,
          "InflationRateFormatted": "0.91",
          "Month": "/Date(-181526400000)/",
          "MonthFormatted": "1964-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3663003663003663003663003663,
          "InflationRateRounded": 0.37,
          "InflationRateFormatted": "0.37",
          "Month": "/Date(-184204800000)/",
          "MonthFormatted": "1964-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-186710400000)/",
          "MonthFormatted": "1964-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5524861878453038674033149171,
          "InflationRateRounded": 0.55,
          "InflationRateFormatted": "0.55",
          "Month": "/Date(-189388800000)/",
          "MonthFormatted": "1964-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1845018450184501845018450185,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(-192067200000)/",
          "MonthFormatted": "1963-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1848428835489833641404805915,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(-194659200000)/",
          "MonthFormatted": "1963-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3710575139146567717996289425,
          "InflationRateRounded": 0.37,
          "InflationRateFormatted": "0.37",
          "Month": "/Date(-197337600000)/",
          "MonthFormatted": "1963-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3724394785847299813780260708,
          "InflationRateRounded": 0.37,
          "InflationRateFormatted": "0.37",
          "Month": "/Date(-199929600000)/",
          "MonthFormatted": "1963-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3710575139146567717996289425,
          "InflationRateRounded": -0.37,
          "InflationRateFormatted": "-0.37",
          "Month": "/Date(-202608000000)/",
          "MonthFormatted": "1963-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.5535055350553505535055350554,
          "InflationRateRounded": -0.55,
          "InflationRateFormatted": "-0.55",
          "Month": "/Date(-205286400000)/",
          "MonthFormatted": "1963-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-207878400000)/",
          "MonthFormatted": "1963-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-210556800000)/",
          "MonthFormatted": "1963-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1848428835489833641404805915,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(-213148800000)/",
          "MonthFormatted": "1963-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1851851851851851851851851852,
          "InflationRateRounded": 0.19,
          "InflationRateFormatted": "0.19",
          "Month": "/Date(-215827200000)/",
          "MonthFormatted": "1963-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9345794392523364485981308411,
          "InflationRateRounded": 0.93,
          "InflationRateFormatted": "0.93",
          "Month": "/Date(-218246400000)/",
          "MonthFormatted": "1963-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3752345215759849906191369606,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(-220924800000)/",
          "MonthFormatted": "1963-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3766478342749529190207156309,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(-223603200000)/",
          "MonthFormatted": "1962-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3780718336483931947069943289,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(-226195200000)/",
          "MonthFormatted": "1962-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-228873600000)/",
          "MonthFormatted": "1962-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.1886792452830188679245283019,
          "InflationRateRounded": -0.19,
          "InflationRateFormatted": "-0.19",
          "Month": "/Date(-231465600000)/",
          "MonthFormatted": "1962-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.7490636704119850187265917603,
          "InflationRateRounded": -0.75,
          "InflationRateFormatted": "-0.75",
          "Month": "/Date(-234144000000)/",
          "MonthFormatted": "1962-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.373134328358208955223880597,
          "InflationRateRounded": -0.37,
          "InflationRateFormatted": "-0.37",
          "Month": "/Date(-236822400000)/",
          "MonthFormatted": "1962-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5628517823639774859287054409,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(-239414400000)/",
          "MonthFormatted": "1962-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3766478342749529190207156309,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(-242092800000)/",
          "MonthFormatted": "1962-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.3358778625954198473282442748,
          "InflationRateRounded": 1.34,
          "InflationRateFormatted": "1.34",
          "Month": "/Date(-244684800000)/",
          "MonthFormatted": "1962-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.38314176245210727969348659,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(-247363200000)/",
          "MonthFormatted": "1962-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1919385796545105566218809981,
          "InflationRateRounded": 0.19,
          "InflationRateFormatted": "0.19",
          "Month": "/Date(-249782400000)/",
          "MonthFormatted": "1962-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1923076923076923076923076923,
          "InflationRateRounded": 0.19,
          "InflationRateFormatted": "0.19",
          "Month": "/Date(-252460800000)/",
          "MonthFormatted": "1962-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1926782273603082851637764933,
          "InflationRateRounded": 0.19,
          "InflationRateFormatted": "0.19",
          "Month": "/Date(-255139200000)/",
          "MonthFormatted": "1961-12-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1695906432748538011695906433,
          "InflationRateRounded": 1.17,
          "InflationRateFormatted": "1.17",
          "Month": "/Date(-257731200000)/",
          "MonthFormatted": "1961-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-260409600000)/",
          "MonthFormatted": "1961-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-263001600000)/",
          "MonthFormatted": "1961-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7858546168958742632612966601,
          "InflationRateRounded": 0.79,
          "InflationRateFormatted": "0.79",
          "Month": "/Date(-265680000000)/",
          "MonthFormatted": "1961-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-268358400000)/",
          "MonthFormatted": "1961-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9920634920634920634920634921,
          "InflationRateRounded": 0.99,
          "InflationRateFormatted": "0.99",
          "Month": "/Date(-270950400000)/",
          "MonthFormatted": "1961-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.1988071570576540755467196819,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(-273628800000)/",
          "MonthFormatted": "1961-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6,
          "InflationRateRounded": 0.6,
          "InflationRateFormatted": "0.60",
          "Month": "/Date(-276220800000)/",
          "MonthFormatted": "1961-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4016064257028112449799196787,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(-278899200000)/",
          "MonthFormatted": "1961-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-281318400000)/",
          "MonthFormatted": "1961-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-283996800000)/",
          "MonthFormatted": "1961-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2012072434607645875251509054,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(-286675200000)/",
          "MonthFormatted": "1960-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6072874493927125506072874494,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(-289267200000)/",
          "MonthFormatted": "1960-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8163265306122448979591836735,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(-291945600000)/",
          "MonthFormatted": "1960-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-294537600000)/",
          "MonthFormatted": "1960-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6085192697768762677484787018,
          "InflationRateRounded": -0.61,
          "InflationRateFormatted": "-0.61",
          "Month": "/Date(-297216000000)/",
          "MonthFormatted": "1960-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2032520325203252032520325203,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(-299894400000)/",
          "MonthFormatted": "1960-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6134969325153374233128834356,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(-302486400000)/",
          "MonthFormatted": "1960-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-305164800000)/",
          "MonthFormatted": "1960-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4106776180698151950718685832,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(-307756800000)/",
          "MonthFormatted": "1960-04-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2049180327868852459016393443,
          "InflationRateRounded": -0.20,
          "InflationRateFormatted": "-0.20",
          "Month": "/Date(-310435200000)/",
          "MonthFormatted": "1960-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-312940800000)/",
          "MonthFormatted": "1960-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2044989775051124744376278119,
          "InflationRateRounded": -0.20,
          "InflationRateFormatted": "-0.20",
          "Month": "/Date(-315619200000)/",
          "MonthFormatted": "1960-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2049180327868852459016393443,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(-318297600000)/",
          "MonthFormatted": "1959-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6185567010309278350515463918,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(-320889600000)/",
          "MonthFormatted": "1959-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6224066390041493775933609959,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(-323568000000)/",
          "MonthFormatted": "1959-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6185567010309278350515463918,
          "InflationRateRounded": -0.62,
          "InflationRateFormatted": "-0.62",
          "Month": "/Date(-326160000000)/",
          "MonthFormatted": "1959-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2066115702479338842975206612,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(-328838400000)/",
          "MonthFormatted": "1959-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2061855670103092783505154639,
          "InflationRateRounded": -0.21,
          "InflationRateFormatted": "-0.21",
          "Month": "/Date(-331516800000)/",
          "MonthFormatted": "1959-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2066115702479338842975206612,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(-334108800000)/",
          "MonthFormatted": "1959-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4115226337448559670781893004,
          "InflationRateRounded": -0.41,
          "InflationRateFormatted": "-0.41",
          "Month": "/Date(-336787200000)/",
          "MonthFormatted": "1959-05-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6134969325153374233128834356,
          "InflationRateRounded": -0.61,
          "InflationRateFormatted": "-0.61",
          "Month": "/Date(-339379200000)/",
          "MonthFormatted": "1959-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-342057600000)/",
          "MonthFormatted": "1959-03-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2040816326530612244897959184,
          "InflationRateRounded": -0.20,
          "InflationRateFormatted": "-0.20",
          "Month": "/Date(-344476800000)/",
          "MonthFormatted": "1959-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2044989775051124744376278119,
          "InflationRateRounded": 0.20,
          "InflationRateFormatted": "0.20",
          "Month": "/Date(-347155200000)/",
          "MonthFormatted": "1959-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4106776180698151950718685832,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(-349833600000)/",
          "MonthFormatted": "1958-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4123711340206185567010309278,
          "InflationRateRounded": 0.41,
          "InflationRateFormatted": "0.41",
          "Month": "/Date(-352425600000)/",
          "MonthFormatted": "1958-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8316008316008316008316008316,
          "InflationRateRounded": 0.83,
          "InflationRateFormatted": "0.83",
          "Month": "/Date(-355104000000)/",
          "MonthFormatted": "1958-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-357696000000)/",
          "MonthFormatted": "1958-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-360374400000)/",
          "MonthFormatted": "1958-08-01",
          "Country": 6
      },
      {
          "InflationRate": -1.6359918200408997955010224949,
          "InflationRateRounded": -1.64,
          "InflationRateFormatted": "-1.64",
          "Month": "/Date(-363052800000)/",
          "MonthFormatted": "1958-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8247422680412371134020618557,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(-365644800000)/",
          "MonthFormatted": "1958-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2057613168724279835390946502,
          "InflationRateRounded": -0.21,
          "InflationRateFormatted": "-0.21",
          "Month": "/Date(-368323200000)/",
          "MonthFormatted": "1958-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0395010395010395010395010395,
          "InflationRateRounded": 1.04,
          "InflationRateFormatted": "1.04",
          "Month": "/Date(-370915200000)/",
          "MonthFormatted": "1958-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8385744234800838574423480084,
          "InflationRateRounded": 0.84,
          "InflationRateFormatted": "0.84",
          "Month": "/Date(-373593600000)/",
          "MonthFormatted": "1958-03-01",
          "Country": 6
      },
      {
          "InflationRate": -0.625,
          "InflationRateRounded": -0.62,
          "InflationRateFormatted": "-0.63",
          "Month": "/Date(-376012800000)/",
          "MonthFormatted": "1958-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-378691200000)/",
          "MonthFormatted": "1958-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.41841004184100418410041841,
          "InflationRateRounded": 0.42,
          "InflationRateFormatted": "0.42",
          "Month": "/Date(-381369600000)/",
          "MonthFormatted": "1957-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6315789473684210526315789474,
          "InflationRateRounded": 0.63,
          "InflationRateFormatted": "0.63",
          "Month": "/Date(-383961600000)/",
          "MonthFormatted": "1957-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8492569002123142250530785563,
          "InflationRateRounded": 0.85,
          "InflationRateFormatted": "0.85",
          "Month": "/Date(-386640000000)/",
          "MonthFormatted": "1957-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2118644067796610169491525424,
          "InflationRateRounded": -0.21,
          "InflationRateFormatted": "-0.21",
          "Month": "/Date(-389232000000)/",
          "MonthFormatted": "1957-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2114164904862579281183932347,
          "InflationRateRounded": -0.21,
          "InflationRateFormatted": "-0.21",
          "Month": "/Date(-391910400000)/",
          "MonthFormatted": "1957-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8528784648187633262260127932,
          "InflationRateRounded": 0.85,
          "InflationRateFormatted": "0.85",
          "Month": "/Date(-394588800000)/",
          "MonthFormatted": "1957-07-01",
          "Country": 6
      },
      {
          "InflationRate": 1.0775862068965517241379310345,
          "InflationRateRounded": 1.08,
          "InflationRateFormatted": "1.08",
          "Month": "/Date(-397180800000)/",
          "MonthFormatted": "1957-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-399859200000)/",
          "MonthFormatted": "1957-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4329004329004329004329004329,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(-402451200000)/",
          "MonthFormatted": "1957-04-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2159827213822894168466522678,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(-405129600000)/",
          "MonthFormatted": "1957-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-407548800000)/",
          "MonthFormatted": "1957-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8714596949891067538126361656,
          "InflationRateRounded": 0.87,
          "InflationRateFormatted": "0.87",
          "Month": "/Date(-410227200000)/",
          "MonthFormatted": "1957-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4376367614879649890590809628,
          "InflationRateRounded": 0.44,
          "InflationRateFormatted": "0.44",
          "Month": "/Date(-412905600000)/",
          "MonthFormatted": "1956-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2192982456140350877192982456,
          "InflationRateRounded": 0.22,
          "InflationRateFormatted": "0.22",
          "Month": "/Date(-415497600000)/",
          "MonthFormatted": "1956-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6622516556291390728476821192,
          "InflationRateRounded": 0.66,
          "InflationRateFormatted": "0.66",
          "Month": "/Date(-418176000000)/",
          "MonthFormatted": "1956-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2202643171806167400881057269,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(-420768000000)/",
          "MonthFormatted": "1956-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2207505518763796909492273731,
          "InflationRateRounded": 0.22,
          "InflationRateFormatted": "0.22",
          "Month": "/Date(-423446400000)/",
          "MonthFormatted": "1956-08-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2202643171806167400881057269,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(-426124800000)/",
          "MonthFormatted": "1956-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2197802197802197802197802198,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(-428716800000)/",
          "MonthFormatted": "1956-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2192982456140350877192982456,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(-431395200000)/",
          "MonthFormatted": "1956-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.5590200445434298440979955457,
          "InflationRateRounded": 1.56,
          "InflationRateFormatted": "1.56",
          "Month": "/Date(-433987200000)/",
          "MonthFormatted": "1956-04-01",
          "Country": 6
      },
      {
          "InflationRate": 1.1261261261261261261261261261,
          "InflationRateRounded": 1.13,
          "InflationRateFormatted": "1.13",
          "Month": "/Date(-436665600000)/",
          "MonthFormatted": "1956-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-439171200000)/",
          "MonthFormatted": "1956-02-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2247191011235955056179775281,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(-441849600000)/",
          "MonthFormatted": "1956-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-444528000000)/",
          "MonthFormatted": "1955-12-01",
          "Country": 6
      },
      {
          "InflationRate": 1.5981735159817351598173515982,
          "InflationRateRounded": 1.60,
          "InflationRateFormatted": "1.60",
          "Month": "/Date(-447120000000)/",
          "MonthFormatted": "1955-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9216589861751152073732718894,
          "InflationRateRounded": 0.92,
          "InflationRateFormatted": "0.92",
          "Month": "/Date(-449798400000)/",
          "MonthFormatted": "1955-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6960556844547563805104408353,
          "InflationRateRounded": 0.70,
          "InflationRateFormatted": "0.70",
          "Month": "/Date(-452390400000)/",
          "MonthFormatted": "1955-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6912442396313364055299539171,
          "InflationRateRounded": -0.69,
          "InflationRateFormatted": "-0.69",
          "Month": "/Date(-455068800000)/",
          "MonthFormatted": "1955-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2309468822170900692840646651,
          "InflationRateRounded": 0.23,
          "InflationRateFormatted": "0.23",
          "Month": "/Date(-457747200000)/",
          "MonthFormatted": "1955-07-01",
          "Country": 6
      },
      {
          "InflationRate": 2.1226415094339622641509433962,
          "InflationRateRounded": 2.12,
          "InflationRateFormatted": "2.12",
          "Month": "/Date(-460339200000)/",
          "MonthFormatted": "1955-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2352941176470588235294117647,
          "InflationRateRounded": -0.24,
          "InflationRateFormatted": "-0.24",
          "Month": "/Date(-463017600000)/",
          "MonthFormatted": "1955-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7109004739336492890995260664,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(-465609600000)/",
          "MonthFormatted": "1955-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-468288000000)/",
          "MonthFormatted": "1955-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-470707200000)/",
          "MonthFormatted": "1955-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2375296912114014251781472684,
          "InflationRateRounded": 0.24,
          "InflationRateFormatted": "0.24",
          "Month": "/Date(-473385600000)/",
          "MonthFormatted": "1955-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7177033492822966507177033493,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(-476064000000)/",
          "MonthFormatted": "1954-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2398081534772182254196642686,
          "InflationRateRounded": 0.24,
          "InflationRateFormatted": "0.24",
          "Month": "/Date(-478656000000)/",
          "MonthFormatted": "1954-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4819277108433734939759036145,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(-481334400000)/",
          "MonthFormatted": "1954-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-483926400000)/",
          "MonthFormatted": "1954-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.7177033492822966507177033493,
          "InflationRateRounded": -0.72,
          "InflationRateFormatted": "-0.72",
          "Month": "/Date(-486604800000)/",
          "MonthFormatted": "1954-08-01",
          "Country": 6
      },
      {
          "InflationRate": 1.7031630170316301703163017032,
          "InflationRateRounded": 1.70,
          "InflationRateFormatted": "1.70",
          "Month": "/Date(-489283200000)/",
          "MonthFormatted": "1954-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4889975550122249388753056235,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(-491875200000)/",
          "MonthFormatted": "1954-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2439024390243902439024390244,
          "InflationRateRounded": -0.24,
          "InflationRateFormatted": "-0.24",
          "Month": "/Date(-494553600000)/",
          "MonthFormatted": "1954-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4901960784313725490196078431,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(-497145600000)/",
          "MonthFormatted": "1954-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7407407407407407407407407407,
          "InflationRateRounded": 0.74,
          "InflationRateFormatted": "0.74",
          "Month": "/Date(-499824000000)/",
          "MonthFormatted": "1954-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-502243200000)/",
          "MonthFormatted": "1954-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-504921600000)/",
          "MonthFormatted": "1954-01-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2463054187192118226600985222,
          "InflationRateRounded": -0.25,
          "InflationRateFormatted": "-0.25",
          "Month": "/Date(-507600000000)/",
          "MonthFormatted": "1953-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2469135802469135802469135802,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(-510192000000)/",
          "MonthFormatted": "1953-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-512870400000)/",
          "MonthFormatted": "1953-10-01",
          "Country": 6
      },
      {
          "InflationRate": -0.2463054187192118226600985222,
          "InflationRateRounded": -0.25,
          "InflationRateFormatted": "-0.25",
          "Month": "/Date(-515462400000)/",
          "MonthFormatted": "1953-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4901960784313725490196078431,
          "InflationRateRounded": -0.49,
          "InflationRateFormatted": "-0.49",
          "Month": "/Date(-518140800000)/",
          "MonthFormatted": "1953-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-520819200000)/",
          "MonthFormatted": "1953-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.4926108374384236453201970443,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(-523411200000)/",
          "MonthFormatted": "1953-06-01",
          "Country": 6
      },
      {
          "InflationRate": -0.4901960784313725490196078431,
          "InflationRateRounded": -0.49,
          "InflationRateFormatted": "-0.49",
          "Month": "/Date(-526089600000)/",
          "MonthFormatted": "1953-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.9900990099009900990099009901,
          "InflationRateRounded": 0.99,
          "InflationRateFormatted": "0.99",
          "Month": "/Date(-528681600000)/",
          "MonthFormatted": "1953-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7481296758104738154613466334,
          "InflationRateRounded": 0.75,
          "InflationRateFormatted": "0.75",
          "Month": "/Date(-531360000000)/",
          "MonthFormatted": "1953-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.25,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(-533779200000)/",
          "MonthFormatted": "1953-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-536457600000)/",
          "MonthFormatted": "1953-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5025125628140703517587939698,
          "InflationRateRounded": 0.50,
          "InflationRateFormatted": "0.50",
          "Month": "/Date(-539136000000)/",
          "MonthFormatted": "1952-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-541728000000)/",
          "MonthFormatted": "1952-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.7594936708860759493670886076,
          "InflationRateRounded": 0.76,
          "InflationRateFormatted": "0.76",
          "Month": "/Date(-544406400000)/",
          "MonthFormatted": "1952-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-546998400000)/",
          "MonthFormatted": "1952-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.7537688442211055276381909548,
          "InflationRateRounded": -0.75,
          "InflationRateFormatted": "-0.75",
          "Month": "/Date(-549676800000)/",
          "MonthFormatted": "1952-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-552355200000)/",
          "MonthFormatted": "1952-07-01",
          "Country": 6
      },
      {
          "InflationRate": 1.5306122448979591836734693878,
          "InflationRateRounded": 1.53,
          "InflationRateFormatted": "1.53",
          "Month": "/Date(-554947200000)/",
          "MonthFormatted": "1952-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-557625600000)/",
          "MonthFormatted": "1952-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.8181818181818181818181818182,
          "InflationRateRounded": 1.82,
          "InflationRateFormatted": "1.82",
          "Month": "/Date(-560217600000)/",
          "MonthFormatted": "1952-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2604166666666666666666666667,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(-562896000000)/",
          "MonthFormatted": "1952-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2610966057441253263707571802,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(-565401600000)/",
          "MonthFormatted": "1952-02-01",
          "Country": 6
      },
      {
          "InflationRate": 1.5915119363395225464190981432,
          "InflationRateRounded": 1.59,
          "InflationRateFormatted": "1.59",
          "Month": "/Date(-568080000000)/",
          "MonthFormatted": "1952-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8021390374331550802139037433,
          "InflationRateRounded": 0.80,
          "InflationRateFormatted": "0.80",
          "Month": "/Date(-570758400000)/",
          "MonthFormatted": "1951-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5376344086021505376344086022,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(-573350400000)/",
          "MonthFormatted": "1951-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5405405405405405405405405405,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(-576028800000)/",
          "MonthFormatted": "1951-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8174386920980926430517711172,
          "InflationRateRounded": 0.82,
          "InflationRateFormatted": "0.82",
          "Month": "/Date(-578620800000)/",
          "MonthFormatted": "1951-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2732240437158469945355191257,
          "InflationRateRounded": 0.27,
          "InflationRateFormatted": "0.27",
          "Month": "/Date(-581299200000)/",
          "MonthFormatted": "1951-08-01",
          "Country": 6
      },
      {
          "InflationRate": 1.6666666666666666666666666667,
          "InflationRateRounded": 1.67,
          "InflationRateFormatted": "1.67",
          "Month": "/Date(-583977600000)/",
          "MonthFormatted": "1951-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.2785515320334261838440111421,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(-586569600000)/",
          "MonthFormatted": "1951-06-01",
          "Country": 6
      },
      {
          "InflationRate": 2.5714285714285714285714285714,
          "InflationRateRounded": 2.57,
          "InflationRateFormatted": "2.57",
          "Month": "/Date(-589248000000)/",
          "MonthFormatted": "1951-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.4492753623188405797101449275,
          "InflationRateRounded": 1.45,
          "InflationRateFormatted": "1.45",
          "Month": "/Date(-591840000000)/",
          "MonthFormatted": "1951-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8771929824561403508771929825,
          "InflationRateRounded": 0.88,
          "InflationRateFormatted": "0.88",
          "Month": "/Date(-594518400000)/",
          "MonthFormatted": "1951-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8849557522123893805309734513,
          "InflationRateRounded": 0.88,
          "InflationRateFormatted": "0.88",
          "Month": "/Date(-596937600000)/",
          "MonthFormatted": "1951-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.8928571428571428571428571429,
          "InflationRateRounded": 0.89,
          "InflationRateFormatted": "0.89",
          "Month": "/Date(-599616000000)/",
          "MonthFormatted": "1951-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.5988023952095808383233532934,
          "InflationRateRounded": 0.60,
          "InflationRateFormatted": "0.60",
          "Month": "/Date(-602294400000)/",
          "MonthFormatted": "1950-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3003003003003003003003003003,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(-604886400000)/",
          "MonthFormatted": "1950-11-01",
          "Country": 6
      },
      {
          "InflationRate": 1.2158054711246200607902735562,
          "InflationRateRounded": 1.22,
          "InflationRateFormatted": "1.22",
          "Month": "/Date(-607564800000)/",
          "MonthFormatted": "1950-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6116207951070336391437308869,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(-610156800000)/",
          "MonthFormatted": "1950-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6079027355623100303951367781,
          "InflationRateRounded": -0.61,
          "InflationRateFormatted": "-0.61",
          "Month": "/Date(-612835200000)/",
          "MonthFormatted": "1950-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-615513600000)/",
          "MonthFormatted": "1950-07-01",
          "Country": 6
      },
      {
          "InflationRate": -0.303030303030303030303030303,
          "InflationRateRounded": -0.30,
          "InflationRateFormatted": "-0.30",
          "Month": "/Date(-618105600000)/",
          "MonthFormatted": "1950-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3039513677811550151975683891,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(-620784000000)/",
          "MonthFormatted": "1950-05-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3048780487804878048780487805,
          "InflationRateRounded": 0.30,
          "InflationRateFormatted": "0.30",
          "Month": "/Date(-623376000000)/",
          "MonthFormatted": "1950-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3058103975535168195718654434,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(-626054400000)/",
          "MonthFormatted": "1950-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3067484662576687116564417178,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(-628473600000)/",
          "MonthFormatted": "1950-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-631152000000)/",
          "MonthFormatted": "1950-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3076923076923076923076923077,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(-633830400000)/",
          "MonthFormatted": "1949-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-636422400000)/",
          "MonthFormatted": "1949-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6191950464396284829721362229,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(-639100800000)/",
          "MonthFormatted": "1949-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3105590062111801242236024845,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(-641692800000)/",
          "MonthFormatted": "1949-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-644371200000)/",
          "MonthFormatted": "1949-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.311526479750778816199376947,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(-647049600000)/",
          "MonthFormatted": "1949-07-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3125,
          "InflationRateRounded": 0.31,
          "InflationRateFormatted": "0.31",
          "Month": "/Date(-649641600000)/",
          "MonthFormatted": "1949-06-01",
          "Country": 6
      },
      {
          "InflationRate": 1.9108280254777070063694267516,
          "InflationRateRounded": 1.91,
          "InflationRateFormatted": "1.91",
          "Month": "/Date(-652320000000)/",
          "MonthFormatted": "1949-05-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3174603174603174603174603175,
          "InflationRateRounded": -0.32,
          "InflationRateFormatted": "-0.32",
          "Month": "/Date(-654912000000)/",
          "MonthFormatted": "1949-04-01",
          "Country": 6
      },
      {
          "InflationRate": -0.3164556962025316455696202532,
          "InflationRateRounded": -0.32,
          "InflationRateFormatted": "-0.32",
          "Month": "/Date(-657590400000)/",
          "MonthFormatted": "1949-03-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3174603174603174603174603175,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(-660009600000)/",
          "MonthFormatted": "1949-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-662688000000)/",
          "MonthFormatted": "1949-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3184713375796178343949044586,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(-665366400000)/",
          "MonthFormatted": "1948-12-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3194888178913738019169329073,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(-667958400000)/",
          "MonthFormatted": "1948-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-670636800000)/",
          "MonthFormatted": "1948-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3205128205128205128205128205,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(-673228800000)/",
          "MonthFormatted": "1948-09-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-675907200000)/",
          "MonthFormatted": "1948-08-01",
          "Country": 6
      },
      {
          "InflationRate": -1.577287066246056782334384858,
          "InflationRateRounded": -1.58,
          "InflationRateFormatted": "-1.58",
          "Month": "/Date(-678585600000)/",
          "MonthFormatted": "1948-07-01",
          "Country": 6
      },
      {
          "InflationRate": 1.6025641025641025641025641026,
          "InflationRateRounded": 1.60,
          "InflationRateFormatted": "1.60",
          "Month": "/Date(-681177600000)/",
          "MonthFormatted": "1948-06-01",
          "Country": 6
      },
      {
          "InflationRate": 0,
          "InflationRateRounded": 0,
          "InflationRateFormatted": "0.00",
          "Month": "/Date(-683856000000)/",
          "MonthFormatted": "1948-05-01",
          "Country": 6
      },
      {
          "InflationRate": 1.2987012987012987012987012987,
          "InflationRateRounded": 1.30,
          "InflationRateFormatted": "1.30",
          "Month": "/Date(-686448000000)/",
          "MonthFormatted": "1948-04-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3257328990228013029315960912,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(-689126400000)/",
          "MonthFormatted": "1948-03-01",
          "Country": 6
      },
      {
          "InflationRate": 1.9933554817275747508305647841,
          "InflationRateRounded": 1.99,
          "InflationRateFormatted": "1.99",
          "Month": "/Date(-691632000000)/",
          "MonthFormatted": "1948-02-01",
          "Country": 6
      },
      {
          "InflationRate": 0.3333333333333333333333333333,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(-694310400000)/",
          "MonthFormatted": "1948-01-01",
          "Country": 6
      },
      {
          "InflationRate": 0.334448160535117056856187291,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(-696988800000)/",
          "MonthFormatted": "1947-12-01",
          "Country": 6
      },
      {
          "InflationRate": 2.0477815699658703071672354949,
          "InflationRateRounded": 2.05,
          "InflationRateFormatted": "2.05",
          "Month": "/Date(-699580800000)/",
          "MonthFormatted": "1947-11-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6872852233676975945017182131,
          "InflationRateRounded": 0.69,
          "InflationRateFormatted": "0.69",
          "Month": "/Date(-702259200000)/",
          "MonthFormatted": "1947-10-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6920415224913494809688581315,
          "InflationRateRounded": 0.69,
          "InflationRateFormatted": "0.69",
          "Month": "/Date(-704851200000)/",
          "MonthFormatted": "1947-09-01",
          "Country": 6
      },
      {
          "InflationRate": -0.6872852233676975945017182131,
          "InflationRateRounded": -0.69,
          "InflationRateFormatted": "-0.69",
          "Month": "/Date(-707529600000)/",
          "MonthFormatted": "1947-08-01",
          "Country": 6
      },
      {
          "InflationRate": 0.6920415224913494809688581315,
          "InflationRateRounded": 0.69,
          "InflationRateFormatted": "0.69",
          "Month": "/Date(-710208000000)/",
          "MonthFormatted": "1947-07-01",
          "Country": 6
      }
    ];
    this.newCountryData = [
      {
          "InflationRate": 0.8800066569222055232657600822,
          "InflationRateRounded": 0.88,
          "InflationRateFormatted": "0.88",
          "Month": "/Date(1546300800000)/",
          "MonthFormatted": "2019-01-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7799908209802036730534334593,
          "InflationRateRounded": 0.78,
          "InflationRateFormatted": "0.78",
          "Month": "/Date(1543622400000)/",
          "MonthFormatted": "2018-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5600031979416884405310036813,
          "InflationRateRounded": 0.56,
          "InflationRateFormatted": "0.56",
          "Month": "/Date(1541030400000)/",
          "MonthFormatted": "2018-11-01",
          "Country": 11
      },
      {
          "InflationRate": 0.510003672342995922679601999,
          "InflationRateRounded": 0.51,
          "InflationRateFormatted": "0.51",
          "Month": "/Date(1538352000000)/",
          "MonthFormatted": "2018-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8099949339837635867272215678,
          "InflationRateRounded": 0.81,
          "InflationRateFormatted": "0.81",
          "Month": "/Date(1535760000000)/",
          "MonthFormatted": "2018-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.1300104048265613883999011288,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1533081600000)/",
          "MonthFormatted": "2018-08-01",
          "Country": 11
      },
      {
          "InflationRate": -0.160002184078893101454912498,
          "InflationRateRounded": -0.16,
          "InflationRateFormatted": "-0.16",
          "Month": "/Date(1530403200000)/",
          "MonthFormatted": "2018-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4299910227917577275155437557,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(1527811200000)/",
          "MonthFormatted": "2018-06-01",
          "Country": 11
      },
      {
          "InflationRate": -0.2799903659228929757255664321,
          "InflationRateRounded": -0.28,
          "InflationRateFormatted": "-0.28",
          "Month": "/Date(1525132800000)/",
          "MonthFormatted": "2018-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2899836707027759805280833102,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(1522540800000)/",
          "MonthFormatted": "2018-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7799993758887181639204994008,
          "InflationRateRounded": 0.78,
          "InflationRateFormatted": "0.78",
          "Month": "/Date(1519862400000)/",
          "MonthFormatted": "2018-03-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9100163682941379949967664473,
          "InflationRateRounded": 0.91,
          "InflationRateFormatted": "0.91",
          "Month": "/Date(1517443200000)/",
          "MonthFormatted": "2018-02-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7499894247639828296048523602,
          "InflationRateRounded": 0.75,
          "InflationRateFormatted": "0.75",
          "Month": "/Date(1514764800000)/",
          "MonthFormatted": "2018-01-01",
          "Country": 11
      },
      {
          "InflationRate": 0.1699973627641267618727789826,
          "InflationRateRounded": 0.17,
          "InflationRateFormatted": "0.17",
          "Month": "/Date(1512086400000)/",
          "MonthFormatted": "2017-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.449998173579799474943897136,
          "InflationRateRounded": 0.45,
          "InflationRateFormatted": "0.45",
          "Month": "/Date(1509494400000)/",
          "MonthFormatted": "2017-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2000048860395563476082836372,
          "InflationRateRounded": 1.20,
          "InflationRateFormatted": "1.20",
          "Month": "/Date(1506816000000)/",
          "MonthFormatted": "2017-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2500043504237312714258368765,
          "InflationRateRounded": 0.25,
          "InflationRateFormatted": "0.25",
          "Month": "/Date(1504224000000)/",
          "MonthFormatted": "2017-09-01",
          "Country": 11
      },
      {
          "InflationRate": -0.7899980050232491521348808897,
          "InflationRateRounded": -0.79,
          "InflationRateFormatted": "-0.79",
          "Month": "/Date(1501545600000)/",
          "MonthFormatted": "2017-08-01",
          "Country": 11
      },
      {
          "InflationRate": -0.1100022179283016752943385664,
          "InflationRateRounded": -0.11,
          "InflationRateFormatted": "-0.11",
          "Month": "/Date(1498867200000)/",
          "MonthFormatted": "2017-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7100000804058889273050358208,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(1496275200000)/",
          "MonthFormatted": "2017-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2799992968974632995795737102,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(1493596800000)/",
          "MonthFormatted": "2017-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6600028439582444596381939696,
          "InflationRateRounded": 0.66,
          "InflationRateFormatted": "0.66",
          "Month": "/Date(1491004800000)/",
          "MonthFormatted": "2017-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.279999420500007569311698884,
          "InflationRateRounded": 0.28,
          "InflationRateFormatted": "0.28",
          "Month": "/Date(1488326400000)/",
          "MonthFormatted": "2017-03-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5399958626775546329501072954,
          "InflationRateRounded": 0.54,
          "InflationRateFormatted": "0.54",
          "Month": "/Date(1485907200000)/",
          "MonthFormatted": "2017-02-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8700079289730419869085275833,
          "InflationRateRounded": 0.87,
          "InflationRateFormatted": "0.87",
          "Month": "/Date(1483228800000)/",
          "MonthFormatted": "2017-01-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4799958331909562992631449389,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(1480550400000)/",
          "MonthFormatted": "2016-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7899899387127504162109439654,
          "InflationRateRounded": 0.79,
          "InflationRateFormatted": "0.79",
          "Month": "/Date(1477958400000)/",
          "MonthFormatted": "2016-11-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8000005392769745078718428493,
          "InflationRateRounded": 0.80,
          "InflationRateFormatted": "0.80",
          "Month": "/Date(1475280000000)/",
          "MonthFormatted": "2016-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6700113226535522492788891381,
          "InflationRateRounded": 0.67,
          "InflationRateFormatted": "0.67",
          "Month": "/Date(1472688000000)/",
          "MonthFormatted": "2016-09-01",
          "Country": 11
      },
      {
          "InflationRate": -0.1500025494504452051951871119,
          "InflationRateRounded": -0.15,
          "InflationRateFormatted": "-0.15",
          "Month": "/Date(1470009600000)/",
          "MonthFormatted": "2016-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.34999568221364406889147125,
          "InflationRateRounded": 0.35,
          "InflationRateFormatted": "0.35",
          "Month": "/Date(1467331200000)/",
          "MonthFormatted": "2016-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3899968445973585064723298152,
          "InflationRateRounded": 0.39,
          "InflationRateFormatted": "0.39",
          "Month": "/Date(1464739200000)/",
          "MonthFormatted": "2016-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4900005453406132303748547902,
          "InflationRateRounded": 0.49,
          "InflationRateFormatted": "0.49",
          "Month": "/Date(1462060800000)/",
          "MonthFormatted": "2016-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7100040275587260203091203135,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(1459468800000)/",
          "MonthFormatted": "2016-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8299947443998602686033094608,
          "InflationRateRounded": 0.83,
          "InflationRateFormatted": "0.83",
          "Month": "/Date(1456790400000)/",
          "MonthFormatted": "2016-03-01",
          "Country": 11
      },
      {
          "InflationRate": 2.9100014229222378373302394631,
          "InflationRateRounded": 2.91,
          "InflationRateFormatted": "2.91",
          "Month": "/Date(1454284800000)/",
          "MonthFormatted": "2016-02-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8600000146034115029612067675,
          "InflationRateRounded": 1.86,
          "InflationRateFormatted": "1.86",
          "Month": "/Date(1451606400000)/",
          "MonthFormatted": "2016-01-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2000082021527695289285679318,
          "InflationRateRounded": 1.20,
          "InflationRateFormatted": "1.20",
          "Month": "/Date(1448928000000)/",
          "MonthFormatted": "2015-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4799944462532070474670868111,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(1446336000000)/",
          "MonthFormatted": "2015-11-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8300007842026035152115653227,
          "InflationRateRounded": 0.83,
          "InflationRateFormatted": "0.83",
          "Month": "/Date(1443657600000)/",
          "MonthFormatted": "2015-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3299947391159059625502429603,
          "InflationRateRounded": 1.33,
          "InflationRateFormatted": "1.33",
          "Month": "/Date(1441065600000)/",
          "MonthFormatted": "2015-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2200025354935840038104629214,
          "InflationRateRounded": 0.22,
          "InflationRateFormatted": "0.22",
          "Month": "/Date(1438387200000)/",
          "MonthFormatted": "2015-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2100079671986830409138413078,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(1435708800000)/",
          "MonthFormatted": "2015-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7200045273651406312796808591,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(1433116800000)/",
          "MonthFormatted": "2015-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.649999488326210769825766386,
          "InflationRateRounded": 0.65,
          "InflationRateFormatted": "0.65",
          "Month": "/Date(1430438400000)/",
          "MonthFormatted": "2015-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8999810243204310401594736372,
          "InflationRateRounded": 0.90,
          "InflationRateFormatted": "0.90",
          "Month": "/Date(1427846400000)/",
          "MonthFormatted": "2015-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8300146563305584983242751933,
          "InflationRateRounded": 0.83,
          "InflationRateFormatted": "0.83",
          "Month": "/Date(1425168000000)/",
          "MonthFormatted": "2015-03-01",
          "Country": 11
      },
      {
          "InflationRate": 1.6499996705272223388134307481,
          "InflationRateRounded": 1.65,
          "InflationRateFormatted": "1.65",
          "Month": "/Date(1422748800000)/",
          "MonthFormatted": "2015-02-01",
          "Country": 11
      },
      {
          "InflationRate": 2.360002591702423507476795781,
          "InflationRateRounded": 2.36,
          "InflationRateFormatted": "2.36",
          "Month": "/Date(1420070400000)/",
          "MonthFormatted": "2015-01-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6200037430152661551212469588,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(1417392000000)/",
          "MonthFormatted": "2014-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6199779407047799968131975011,
          "InflationRateRounded": 0.62,
          "InflationRateFormatted": "0.62",
          "Month": "/Date(1414800000000)/",
          "MonthFormatted": "2014-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2400170031221972659666670927,
          "InflationRateRounded": 1.24,
          "InflationRateFormatted": "1.24",
          "Month": "/Date(1412121600000)/",
          "MonthFormatted": "2014-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2299893706455063619189166033,
          "InflationRateRounded": 1.23,
          "InflationRateFormatted": "1.23",
          "Month": "/Date(1409529600000)/",
          "MonthFormatted": "2014-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.800009235069063138585251552,
          "InflationRateRounded": 0.80,
          "InflationRateFormatted": "0.80",
          "Month": "/Date(1406851200000)/",
          "MonthFormatted": "2014-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8499922925925886001515610464,
          "InflationRateRounded": 0.85,
          "InflationRateFormatted": "0.85",
          "Month": "/Date(1404172800000)/",
          "MonthFormatted": "2014-07-01",
          "Country": 11
      },
      {
          "InflationRate": 1.149998037359943824880169921,
          "InflationRateRounded": 1.15,
          "InflationRateFormatted": "1.15",
          "Month": "/Date(1401580800000)/",
          "MonthFormatted": "2014-06-01",
          "Country": 11
      },
      {
          "InflationRate": 2.2200059785081326212325311982,
          "InflationRateRounded": 2.22,
          "InflationRateFormatted": "2.22",
          "Month": "/Date(1398902400000)/",
          "MonthFormatted": "2014-05-01",
          "Country": 11
      },
      {
          "InflationRate": 1.5500059308275360626005637455,
          "InflationRateRounded": 1.55,
          "InflationRateFormatted": "1.55",
          "Month": "/Date(1396310400000)/",
          "MonthFormatted": "2014-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2599977032187257815073349154,
          "InflationRateRounded": 1.26,
          "InflationRateFormatted": "1.26",
          "Month": "/Date(1393632000000)/",
          "MonthFormatted": "2014-03-01",
          "Country": 11
      },
      {
          "InflationRate": 1.989991829349118235981021647,
          "InflationRateRounded": 1.99,
          "InflationRateFormatted": "1.99",
          "Month": "/Date(1391212800000)/",
          "MonthFormatted": "2014-02-01",
          "Country": 11
      },
      {
          "InflationRate": 1.6100090077078614105234352433,
          "InflationRateRounded": 1.61,
          "InflationRateFormatted": "1.61",
          "Month": "/Date(1388534400000)/",
          "MonthFormatted": "2014-01-01",
          "Country": 11
      },
      {
          "InflationRate": 2.3900002529512601837204460504,
          "InflationRateRounded": 2.39,
          "InflationRateFormatted": "2.39",
          "Month": "/Date(1385856000000)/",
          "MonthFormatted": "2013-12-01",
          "Country": 11
      },
      {
          "InflationRate": 1.5500002593419285011592584204,
          "InflationRateRounded": 1.55,
          "InflationRateFormatted": "1.55",
          "Month": "/Date(1383264000000)/",
          "MonthFormatted": "2013-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8499842396637727855092450157,
          "InflationRateRounded": 1.85,
          "InflationRateFormatted": "1.85",
          "Month": "/Date(1380585600000)/",
          "MonthFormatted": "2013-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.670002665047518155316514036,
          "InflationRateRounded": 1.67,
          "InflationRateFormatted": "1.67",
          "Month": "/Date(1377993600000)/",
          "MonthFormatted": "2013-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.1300195631168534497439943741,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1375315200000)/",
          "MonthFormatted": "2013-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9799834596273709548367142483,
          "InflationRateRounded": 0.98,
          "InflationRateFormatted": "0.98",
          "Month": "/Date(1372636800000)/",
          "MonthFormatted": "2013-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3300050879784379724574572614,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(1370044800000)/",
          "MonthFormatted": "2013-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7200067108639427214173511896,
          "InflationRateRounded": 0.72,
          "InflationRateFormatted": "0.72",
          "Month": "/Date(1367366400000)/",
          "MonthFormatted": "2013-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4600025885318587627710312307,
          "InflationRateRounded": 0.46,
          "InflationRateFormatted": "0.46",
          "Month": "/Date(1364774400000)/",
          "MonthFormatted": "2013-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.049998554196028204054140451,
          "InflationRateRounded": 1.05,
          "InflationRateFormatted": "1.05",
          "Month": "/Date(1362096000000)/",
          "MonthFormatted": "2013-03-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2399821666451806970048558291,
          "InflationRateRounded": 1.24,
          "InflationRateFormatted": "1.24",
          "Month": "/Date(1359676800000)/",
          "MonthFormatted": "2013-02-01",
          "Country": 11
      },
      {
          "InflationRate": 3.0100197506695833878581659018,
          "InflationRateRounded": 3.01,
          "InflationRateFormatted": "3.01",
          "Month": "/Date(1356998400000)/",
          "MonthFormatted": "2013-01-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3899828555784447047524157029,
          "InflationRateRounded": 1.39,
          "InflationRateFormatted": "1.39",
          "Month": "/Date(1354320000000)/",
          "MonthFormatted": "2012-12-01",
          "Country": 11
      },
      {
          "InflationRate": 1.690018712062434393683537949,
          "InflationRateRounded": 1.69,
          "InflationRateFormatted": "1.69",
          "Month": "/Date(1351728000000)/",
          "MonthFormatted": "2012-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.7799944200073799902393677479,
          "InflationRateRounded": 1.78,
          "InflationRateFormatted": "1.78",
          "Month": "/Date(1349049600000)/",
          "MonthFormatted": "2012-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2999941768919072325943475854,
          "InflationRateRounded": 1.30,
          "InflationRateFormatted": "1.30",
          "Month": "/Date(1346457600000)/",
          "MonthFormatted": "2012-09-01",
          "Country": 11
      },
      {
          "InflationRate": 2.3300060430525360356130809215,
          "InflationRateRounded": 2.33,
          "InflationRateFormatted": "2.33",
          "Month": "/Date(1343779200000)/",
          "MonthFormatted": "2012-08-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3399973832584044524864687316,
          "InflationRateRounded": 1.34,
          "InflationRateFormatted": "1.34",
          "Month": "/Date(1341100800000)/",
          "MonthFormatted": "2012-07-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8199981492281101196853190936,
          "InflationRateRounded": 1.82,
          "InflationRateFormatted": "1.82",
          "Month": "/Date(1338508800000)/",
          "MonthFormatted": "2012-06-01",
          "Country": 11
      },
      {
          "InflationRate": 1.580000435305276751638466981,
          "InflationRateRounded": 1.58,
          "InflationRateFormatted": "1.58",
          "Month": "/Date(1335830400000)/",
          "MonthFormatted": "2012-05-01",
          "Country": 11
      },
      {
          "InflationRate": 1.7100058070705093859584393418,
          "InflationRateRounded": 1.71,
          "InflationRateFormatted": "1.71",
          "Month": "/Date(1333238400000)/",
          "MonthFormatted": "2012-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.4600079816721017122835069097,
          "InflationRateRounded": 1.46,
          "InflationRateFormatted": "1.46",
          "Month": "/Date(1330560000000)/",
          "MonthFormatted": "2012-03-01",
          "Country": 11
      },
      {
          "InflationRate": 1.5399707877877219465421142727,
          "InflationRateRounded": 1.54,
          "InflationRateFormatted": "1.54",
          "Month": "/Date(1328054400000)/",
          "MonthFormatted": "2012-02-01",
          "Country": 11
      },
      {
          "InflationRate": 1.9300165995038678831774960994,
          "InflationRateRounded": 1.93,
          "InflationRateFormatted": "1.93",
          "Month": "/Date(1325376000000)/",
          "MonthFormatted": "2012-01-01",
          "Country": 11
      },
      {
          "InflationRate": 2.2799824672397894414740750849,
          "InflationRateRounded": 2.28,
          "InflationRateFormatted": "2.28",
          "Month": "/Date(1322697600000)/",
          "MonthFormatted": "2011-12-01",
          "Country": 11
      },
      {
          "InflationRate": 8.110013079764959119680780718,
          "InflationRateRounded": 8.11,
          "InflationRateFormatted": "8.11",
          "Month": "/Date(1320105600000)/",
          "MonthFormatted": "2011-11-01",
          "Country": 11
      },
      {
          "InflationRate": 8.169994954778372924084727485,
          "InflationRateRounded": 8.17,
          "InflationRateFormatted": "8.17",
          "Month": "/Date(1317427200000)/",
          "MonthFormatted": "2011-10-01",
          "Country": 11
      },
      {
          "InflationRate": 13.590032316241428872227257331,
          "InflationRateRounded": 13.59,
          "InflationRateFormatted": "13.59",
          "Month": "/Date(1314835200000)/",
          "MonthFormatted": "2011-09-01",
          "Country": 11
      },
      {
          "InflationRate": 8.929956860015437054115450816,
          "InflationRateRounded": 8.93,
          "InflationRateFormatted": "8.93",
          "Month": "/Date(1312156800000)/",
          "MonthFormatted": "2011-08-01",
          "Country": 11
      },
      {
          "InflationRate": 3.5100422955735450090449045317,
          "InflationRateRounded": 3.51,
          "InflationRateFormatted": "3.51",
          "Month": "/Date(1309478400000)/",
          "MonthFormatted": "2011-07-01",
          "Country": 11
      },
      {
          "InflationRate": 8.619946372701316800772286919,
          "InflationRateRounded": 8.62,
          "InflationRateFormatted": "8.62",
          "Month": "/Date(1306886400000)/",
          "MonthFormatted": "2011-06-01",
          "Country": 11
      },
      {
          "InflationRate": 13.140038461928779468027887435,
          "InflationRateRounded": 13.14,
          "InflationRateFormatted": "13.14",
          "Month": "/Date(1304208000000)/",
          "MonthFormatted": "2011-05-01",
          "Country": 11
      },
      {
          "InflationRate": 4.459987981254998148159190729,
          "InflationRateRounded": 4.46,
          "InflationRateFormatted": "4.46",
          "Month": "/Date(1301616000000)/",
          "MonthFormatted": "2011-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.890013717569289926767622216,
          "InflationRateRounded": 1.89,
          "InflationRateFormatted": "1.89",
          "Month": "/Date(1298937600000)/",
          "MonthFormatted": "2011-03-01",
          "Country": 11
      },
      {
          "InflationRate": 2.6899658858280953533045547723,
          "InflationRateRounded": 2.69,
          "InflationRateFormatted": "2.69",
          "Month": "/Date(1296518400000)/",
          "MonthFormatted": "2011-02-01",
          "Country": 11
      },
      {
          "InflationRate": 1.4200119242472818903306728763,
          "InflationRateRounded": 1.42,
          "InflationRateFormatted": "1.42",
          "Month": "/Date(1293840000000)/",
          "MonthFormatted": "2011-01-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9700081212623735667107751546,
          "InflationRateRounded": 0.97,
          "InflationRateFormatted": "0.97",
          "Month": "/Date(1291161600000)/",
          "MonthFormatted": "2010-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9000181221317070689923163594,
          "InflationRateRounded": 0.90,
          "InflationRateFormatted": "0.90",
          "Month": "/Date(1288569600000)/",
          "MonthFormatted": "2010-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.0499595751811154933818382906,
          "InflationRateRounded": 1.05,
          "InflationRateFormatted": "1.05",
          "Month": "/Date(1285891200000)/",
          "MonthFormatted": "2010-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.6100040744701350164670372424,
          "InflationRateRounded": 1.61,
          "InflationRateFormatted": "1.61",
          "Month": "/Date(1283299200000)/",
          "MonthFormatted": "2010-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6100150801884209607219547713,
          "InflationRateRounded": 0.61,
          "InflationRateFormatted": "0.61",
          "Month": "/Date(1280620800000)/",
          "MonthFormatted": "2010-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2900112353380322871259649107,
          "InflationRateRounded": 0.29,
          "InflationRateFormatted": "0.29",
          "Month": "/Date(1277942400000)/",
          "MonthFormatted": "2010-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.1799848785938430451568009528,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(1275350400000)/",
          "MonthFormatted": "2010-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7899922821582981784394973662,
          "InflationRateRounded": 0.79,
          "InflationRateFormatted": "0.79",
          "Month": "/Date(1272672000000)/",
          "MonthFormatted": "2010-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.66999173259469799832238053,
          "InflationRateRounded": 0.67,
          "InflationRateFormatted": "0.67",
          "Month": "/Date(1270080000000)/",
          "MonthFormatted": "2010-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.120057604706011575860943216,
          "InflationRateRounded": 1.12,
          "InflationRateFormatted": "1.12",
          "Month": "/Date(1267401600000)/",
          "MonthFormatted": "2010-03-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5299468672863055012180879738,
          "InflationRateRounded": 0.53,
          "InflationRateFormatted": "0.53",
          "Month": "/Date(1264982400000)/",
          "MonthFormatted": "2010-02-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7900323911734631968720220177,
          "InflationRateRounded": 0.79,
          "InflationRateFormatted": "0.79",
          "Month": "/Date(1262304000000)/",
          "MonthFormatted": "2010-01-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3300305116815257093809323919,
          "InflationRateRounded": 1.33,
          "InflationRateFormatted": "1.33",
          "Month": "/Date(1259625600000)/",
          "MonthFormatted": "2009-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3199993714718949729536499806,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(1257033600000)/",
          "MonthFormatted": "2009-11-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4299468346160461934437250171,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(1254355200000)/",
          "MonthFormatted": "2009-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3300356159807219301502457653,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(1251763200000)/",
          "MonthFormatted": "2009-09-01",
          "Country": 11
      },
      {
          "InflationRate": -0.2199873299936334007908485271,
          "InflationRateRounded": -0.22,
          "InflationRateFormatted": "-0.22",
          "Month": "/Date(1249084800000)/",
          "MonthFormatted": "2009-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3799586105186371601424051888,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(1246406400000)/",
          "MonthFormatted": "2009-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4300082020082975656757897419,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(1243814400000)/",
          "MonthFormatted": "2009-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3200232627993327922389166182,
          "InflationRateRounded": 0.32,
          "InflationRateFormatted": "0.32",
          "Month": "/Date(1241136000000)/",
          "MonthFormatted": "2009-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4299496560162063501614617807,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(1238544000000)/",
          "MonthFormatted": "2009-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6400494792886221859058310954,
          "InflationRateRounded": 0.64,
          "InflationRateFormatted": "0.64",
          "Month": "/Date(1235865600000)/",
          "MonthFormatted": "2009-03-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2299644448077158854060239487,
          "InflationRateRounded": 1.23,
          "InflationRateFormatted": "1.23",
          "Month": "/Date(1233446400000)/",
          "MonthFormatted": "2009-02-01",
          "Country": 11
      },
      {
          "InflationRate": 4.1200369690171722106008486534,
          "InflationRateRounded": 4.12,
          "InflationRateFormatted": "4.12",
          "Month": "/Date(1230768000000)/",
          "MonthFormatted": "2009-01-01",
          "Country": 11
      },
      {
          "InflationRate": 1.1799351608278403733634136576,
          "InflationRateRounded": 1.18,
          "InflationRateFormatted": "1.18",
          "Month": "/Date(1228089600000)/",
          "MonthFormatted": "2008-12-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3500717798344482116130608754,
          "InflationRateRounded": 1.35,
          "InflationRateFormatted": "1.35",
          "Month": "/Date(1225497600000)/",
          "MonthFormatted": "2008-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.0099540905917447690777921171,
          "InflationRateRounded": 1.01,
          "InflationRateFormatted": "1.01",
          "Month": "/Date(1222819200000)/",
          "MonthFormatted": "2008-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.0300647820441344374364336211,
          "InflationRateRounded": 1.03,
          "InflationRateFormatted": "1.03",
          "Month": "/Date(1220227200000)/",
          "MonthFormatted": "2008-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.1699471067508700310543768294,
          "InflationRateRounded": 0.17,
          "InflationRateFormatted": "0.17",
          "Month": "/Date(1217548800000)/",
          "MonthFormatted": "2008-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7600122968281733996595001088,
          "InflationRateRounded": 0.76,
          "InflationRateFormatted": "0.76",
          "Month": "/Date(1214870400000)/",
          "MonthFormatted": "2008-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5999925848693354505641377153,
          "InflationRateRounded": 0.60,
          "InflationRateFormatted": "0.60",
          "Month": "/Date(1212278400000)/",
          "MonthFormatted": "2008-06-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2800046891986324391800862922,
          "InflationRateRounded": 1.28,
          "InflationRateFormatted": "1.28",
          "Month": "/Date(1209600000000)/",
          "MonthFormatted": "2008-05-01",
          "Country": 11
      },
      {
          "InflationRate": 1.1500226502769447972452816555,
          "InflationRateRounded": 1.15,
          "InflationRateFormatted": "1.15",
          "Month": "/Date(1207008000000)/",
          "MonthFormatted": "2008-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.840008781230586707645742738,
          "InflationRateRounded": 0.84,
          "InflationRateFormatted": "0.84",
          "Month": "/Date(1204329600000)/",
          "MonthFormatted": "2008-03-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6599294558167919980967551744,
          "InflationRateRounded": 0.66,
          "InflationRateFormatted": "0.66",
          "Month": "/Date(1201824000000)/",
          "MonthFormatted": "2008-02-01",
          "Country": 11
      },
      {
          "InflationRate": 2.5400538422376155139696693871,
          "InflationRateRounded": 2.54,
          "InflationRateFormatted": "2.54",
          "Month": "/Date(1199145600000)/",
          "MonthFormatted": "2008-01-01",
          "Country": 11
      },
      {
          "InflationRate": 2.4399398657845401332672199383,
          "InflationRateRounded": 2.44,
          "InflationRateFormatted": "2.44",
          "Month": "/Date(1196467200000)/",
          "MonthFormatted": "2007-12-01",
          "Country": 11
      },
      {
          "InflationRate": 2.2200404194446284302813044721,
          "InflationRateRounded": 2.22,
          "InflationRateFormatted": "2.22",
          "Month": "/Date(1193875200000)/",
          "MonthFormatted": "2007-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.5400044269321923781924686175,
          "InflationRateRounded": 1.54,
          "InflationRateFormatted": "1.54",
          "Month": "/Date(1191196800000)/",
          "MonthFormatted": "2007-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9399985511895768438699796132,
          "InflationRateRounded": 0.94,
          "InflationRateFormatted": "0.94",
          "Month": "/Date(1188604800000)/",
          "MonthFormatted": "2007-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2599905665486828133764098559,
          "InflationRateRounded": 0.26,
          "InflationRateFormatted": "0.26",
          "Month": "/Date(1185926400000)/",
          "MonthFormatted": "2007-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4800041701417848206839032527,
          "InflationRateRounded": 0.48,
          "InflationRateFormatted": "0.48",
          "Month": "/Date(1183248000000)/",
          "MonthFormatted": "2007-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.429999965448394507299864275,
          "InflationRateRounded": 0.43,
          "InflationRateFormatted": "0.43",
          "Month": "/Date(1180656000000)/",
          "MonthFormatted": "2007-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3800090259632020686986364574,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(1177977600000)/",
          "MonthFormatted": "2007-05-01",
          "Country": 11
      },
      {
          "InflationRate": -0.1800054805258786990926918069,
          "InflationRateRounded": -0.18,
          "InflationRateFormatted": "-0.18",
          "Month": "/Date(1175385600000)/",
          "MonthFormatted": "2007-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3799976579237578360563479955,
          "InflationRateRounded": 0.38,
          "InflationRateFormatted": "0.38",
          "Month": "/Date(1172707200000)/",
          "MonthFormatted": "2007-03-01",
          "Country": 11
      },
      {
          "InflationRate": 0.840003453421866383369968419,
          "InflationRateRounded": 0.84,
          "InflationRateFormatted": "0.84",
          "Month": "/Date(1170288000000)/",
          "MonthFormatted": "2007-02-01",
          "Country": 11
      },
      {
          "InflationRate": 1.7599970779821089080589880201,
          "InflationRateRounded": 1.76,
          "InflationRateFormatted": "1.76",
          "Month": "/Date(1167609600000)/",
          "MonthFormatted": "2007-01-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8399992415651915036994401313,
          "InflationRateRounded": 0.84,
          "InflationRateFormatted": "0.84",
          "Month": "/Date(1164931200000)/",
          "MonthFormatted": "2006-12-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8400084962908075260534607126,
          "InflationRateRounded": 1.84,
          "InflationRateFormatted": "1.84",
          "Month": "/Date(1162339200000)/",
          "MonthFormatted": "2006-11-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6999947364615589036328808218,
          "InflationRateRounded": 0.70,
          "InflationRateFormatted": "0.70",
          "Month": "/Date(1159660800000)/",
          "MonthFormatted": "2006-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.1799989364411642050877296098,
          "InflationRateRounded": 0.18,
          "InflationRateFormatted": "0.18",
          "Month": "/Date(1157068800000)/",
          "MonthFormatted": "2006-09-01",
          "Country": 11
      },
      {
          "InflationRate": -0.6600034254116610799833622862,
          "InflationRateRounded": -0.66,
          "InflationRateFormatted": "-0.66",
          "Month": "/Date(1154390400000)/",
          "MonthFormatted": "2006-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5100047563183301577968688782,
          "InflationRateRounded": 0.51,
          "InflationRateFormatted": "0.51",
          "Month": "/Date(1151712000000)/",
          "MonthFormatted": "2006-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2400001972082483246262903694,
          "InflationRateRounded": 0.24,
          "InflationRateFormatted": "0.24",
          "Month": "/Date(1149120000000)/",
          "MonthFormatted": "2006-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.3299973851130415856962310778,
          "InflationRateRounded": 0.33,
          "InflationRateFormatted": "0.33",
          "Month": "/Date(1146441600000)/",
          "MonthFormatted": "2006-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8799986663078399664671685477,
          "InflationRateRounded": 0.88,
          "InflationRateFormatted": "0.88",
          "Month": "/Date(1143849600000)/",
          "MonthFormatted": "2006-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.1299996286698237680610398676,
          "InflationRateRounded": 0.13,
          "InflationRateFormatted": "0.13",
          "Month": "/Date(1141171200000)/",
          "MonthFormatted": "2006-03-01",
          "Country": 11
      },
      {
          "InflationRate": 0.160007079073501233094077179,
          "InflationRateRounded": 0.16,
          "InflationRateFormatted": "0.16",
          "Month": "/Date(1138752000000)/",
          "MonthFormatted": "2006-02-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2999924763353093418682953116,
          "InflationRateRounded": 1.30,
          "InflationRateFormatted": "1.30",
          "Month": "/Date(1136073600000)/",
          "MonthFormatted": "2006-01-01",
          "Country": 11
      },
      {
          "InflationRate": 1.729998641534149827179467404,
          "InflationRateRounded": 1.73,
          "InflationRateFormatted": "1.73",
          "Month": "/Date(1133395200000)/",
          "MonthFormatted": "2005-12-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5000096592908949639284383188,
          "InflationRateRounded": 0.50,
          "InflationRateFormatted": "0.50",
          "Month": "/Date(1130803200000)/",
          "MonthFormatted": "2005-11-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9400036622283946360244370513,
          "InflationRateRounded": 0.94,
          "InflationRateFormatted": "0.94",
          "Month": "/Date(1128124800000)/",
          "MonthFormatted": "2005-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.0499882345635451341890259527,
          "InflationRateRounded": 0.05,
          "InflationRateFormatted": "0.05",
          "Month": "/Date(1125532800000)/",
          "MonthFormatted": "2005-09-01",
          "Country": 11
      },
      {
          "InflationRate": -0.2699930653888157594229739101,
          "InflationRateRounded": -0.27,
          "InflationRateFormatted": "-0.27",
          "Month": "/Date(1122854400000)/",
          "MonthFormatted": "2005-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.8499951242537565680489656538,
          "InflationRateRounded": 0.85,
          "InflationRateFormatted": "0.85",
          "Month": "/Date(1120176000000)/",
          "MonthFormatted": "2005-07-01",
          "Country": 11
      },
      {
          "InflationRate": 0.150002810006306636755577503,
          "InflationRateRounded": 0.15,
          "InflationRateFormatted": "0.15",
          "Month": "/Date(1117584000000)/",
          "MonthFormatted": "2005-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5700010809991324289014096253,
          "InflationRateRounded": 0.57,
          "InflationRateFormatted": "0.57",
          "Month": "/Date(1114905600000)/",
          "MonthFormatted": "2005-05-01",
          "Country": 11
      },
      {
          "InflationRate": 0.5300012163604846639234749335,
          "InflationRateRounded": 0.53,
          "InflationRateFormatted": "0.53",
          "Month": "/Date(1112313600000)/",
          "MonthFormatted": "2005-04-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9900018560623069227208821202,
          "InflationRateRounded": 0.99,
          "InflationRateFormatted": "0.99",
          "Month": "/Date(1109635200000)/",
          "MonthFormatted": "2005-03-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9299933921197168601713998444,
          "InflationRateRounded": 0.93,
          "InflationRateFormatted": "0.93",
          "Month": "/Date(1107216000000)/",
          "MonthFormatted": "2005-02-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7100024786038533149056868226,
          "InflationRateRounded": 0.71,
          "InflationRateFormatted": "0.71",
          "Month": "/Date(1104537600000)/",
          "MonthFormatted": "2005-01-01",
          "Country": 11
      },
      {
          "InflationRate": 2.0999988191125491131725709558,
          "InflationRateRounded": 2.10,
          "InflationRateFormatted": "2.10",
          "Month": "/Date(1101859200000)/",
          "MonthFormatted": "2004-12-01",
          "Country": 11
      },
      {
          "InflationRate": 1.5600048642955675976531192424,
          "InflationRateRounded": 1.56,
          "InflationRateFormatted": "1.56",
          "Month": "/Date(1099267200000)/",
          "MonthFormatted": "2004-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.2299870904878605025976329994,
          "InflationRateRounded": 1.23,
          "InflationRateFormatted": "1.23",
          "Month": "/Date(1096588800000)/",
          "MonthFormatted": "2004-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.0900128231361047718425150594,
          "InflationRateRounded": 0.09,
          "InflationRateFormatted": "0.09",
          "Month": "/Date(1093996800000)/",
          "MonthFormatted": "2004-09-01",
          "Country": 11
      },
      {
          "InflationRate": -0.1800015781603476126411948109,
          "InflationRateRounded": -0.18,
          "InflationRateFormatted": "-0.18",
          "Month": "/Date(1091318400000)/",
          "MonthFormatted": "2004-08-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9100010752811294216000946563,
          "InflationRateRounded": 0.91,
          "InflationRateFormatted": "0.91",
          "Month": "/Date(1088640000000)/",
          "MonthFormatted": "2004-07-01",
          "Country": 11
      },
      {
          "InflationRate": 1.0299908836479024412037839776,
          "InflationRateRounded": 1.03,
          "InflationRateFormatted": "1.03",
          "Month": "/Date(1086048000000)/",
          "MonthFormatted": "2004-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6800087338851852878212771848,
          "InflationRateRounded": 0.68,
          "InflationRateFormatted": "0.68",
          "Month": "/Date(1083369600000)/",
          "MonthFormatted": "2004-05-01",
          "Country": 11
      },
      {
          "InflationRate": 1.289991016492364018509415733,
          "InflationRateRounded": 1.29,
          "InflationRateFormatted": "1.29",
          "Month": "/Date(1080777600000)/",
          "MonthFormatted": "2004-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.1800040583819788083311804335,
          "InflationRateRounded": 1.18,
          "InflationRateFormatted": "1.18",
          "Month": "/Date(1078099200000)/",
          "MonthFormatted": "2004-03-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8500109275445108791950172633,
          "InflationRateRounded": 1.85,
          "InflationRateFormatted": "1.85",
          "Month": "/Date(1075593600000)/",
          "MonthFormatted": "2004-02-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8499929478670179781712645008,
          "InflationRateRounded": 1.85,
          "InflationRateFormatted": "1.85",
          "Month": "/Date(1072915200000)/",
          "MonthFormatted": "2004-01-01",
          "Country": 11
      },
      {
          "InflationRate": 2.0200016755908091085348700756,
          "InflationRateRounded": 2.02,
          "InflationRateFormatted": "2.02",
          "Month": "/Date(1070236800000)/",
          "MonthFormatted": "2003-12-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8599950719988073408878270384,
          "InflationRateRounded": 1.86,
          "InflationRateFormatted": "1.86",
          "Month": "/Date(1067644800000)/",
          "MonthFormatted": "2003-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.9299933261363545568830418587,
          "InflationRateRounded": 1.93,
          "InflationRateFormatted": "1.93",
          "Month": "/Date(1064966400000)/",
          "MonthFormatted": "2003-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.650008083467355434273748346,
          "InflationRateRounded": 1.65,
          "InflationRateFormatted": "1.65",
          "Month": "/Date(1062374400000)/",
          "MonthFormatted": "2003-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.2099972512242801355653187428,
          "InflationRateRounded": 0.21,
          "InflationRateFormatted": "0.21",
          "Month": "/Date(1059696000000)/",
          "MonthFormatted": "2003-08-01",
          "Country": 11
      },
      {
          "InflationRate": 1.4699926811227347825551747828,
          "InflationRateRounded": 1.47,
          "InflationRateFormatted": "1.47",
          "Month": "/Date(1057017600000)/",
          "MonthFormatted": "2003-07-01",
          "Country": 11
      },
      {
          "InflationRate": 1.7500114550297727718460894272,
          "InflationRateRounded": 1.75,
          "InflationRateFormatted": "1.75",
          "Month": "/Date(1054425600000)/",
          "MonthFormatted": "2003-06-01",
          "Country": 11
      },
      {
          "InflationRate": 2.0299968471879391431242410159,
          "InflationRateRounded": 2.03,
          "InflationRateFormatted": "2.03",
          "Month": "/Date(1051747200000)/",
          "MonthFormatted": "2003-05-01",
          "Country": 11
      },
      {
          "InflationRate": 2.0099907953461217185936978649,
          "InflationRateRounded": 2.01,
          "InflationRateFormatted": "2.01",
          "Month": "/Date(1049155200000)/",
          "MonthFormatted": "2003-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8199970495616515599518655138,
          "InflationRateRounded": 1.82,
          "InflationRateFormatted": "1.82",
          "Month": "/Date(1046476800000)/",
          "MonthFormatted": "2003-03-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8500223579806182918571259154,
          "InflationRateRounded": 1.85,
          "InflationRateFormatted": "1.85",
          "Month": "/Date(1044057600000)/",
          "MonthFormatted": "2003-02-01",
          "Country": 11
      },
      {
          "InflationRate": 4.2699915242896016416112771557,
          "InflationRateRounded": 4.27,
          "InflationRateFormatted": "4.27",
          "Month": "/Date(1041379200000)/",
          "MonthFormatted": "2003-01-01",
          "Country": 11
      },
      {
          "InflationRate": 3.2399959398500949916650870031,
          "InflationRateRounded": 3.24,
          "InflationRateFormatted": "3.24",
          "Month": "/Date(1038700800000)/",
          "MonthFormatted": "2002-12-01",
          "Country": 11
      },
      {
          "InflationRate": 3.1800001102437546437805700705,
          "InflationRateRounded": 3.18,
          "InflationRateFormatted": "3.18",
          "Month": "/Date(1036108800000)/",
          "MonthFormatted": "2002-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.6899916596358266479950750295,
          "InflationRateRounded": 1.69,
          "InflationRateFormatted": "1.69",
          "Month": "/Date(1033430400000)/",
          "MonthFormatted": "2002-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.1700098732782627723437918903,
          "InflationRateRounded": 1.17,
          "InflationRateFormatted": "1.17",
          "Month": "/Date(1030838400000)/",
          "MonthFormatted": "2002-09-01",
          "Country": 11
      },
      {
          "InflationRate": 1.1199955390008192340250848153,
          "InflationRateRounded": 1.12,
          "InflationRateFormatted": "1.12",
          "Month": "/Date(1028160000000)/",
          "MonthFormatted": "2002-08-01",
          "Country": 11
      },
      {
          "InflationRate": 1.1900097845204484471825983604,
          "InflationRateRounded": 1.19,
          "InflationRateFormatted": "1.19",
          "Month": "/Date(1025481600000)/",
          "MonthFormatted": "2002-07-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3899891969751530428519985596,
          "InflationRateRounded": 1.39,
          "InflationRateFormatted": "1.39",
          "Month": "/Date(1022889600000)/",
          "MonthFormatted": "2002-06-01",
          "Country": 11
      },
      {
          "InflationRate": 2.2400117813481346890180412264,
          "InflationRateRounded": 2.24,
          "InflationRateFormatted": "2.24",
          "Month": "/Date(1020211200000)/",
          "MonthFormatted": "2002-05-01",
          "Country": 11
      },
      {
          "InflationRate": 2.8600029698356861600536618586,
          "InflationRateRounded": 2.86,
          "InflationRateFormatted": "2.86",
          "Month": "/Date(1017619200000)/",
          "MonthFormatted": "2002-04-01",
          "Country": 11
      },
      {
          "InflationRate": 2.5900040185641906532924647982,
          "InflationRateRounded": 2.59,
          "InflationRateFormatted": "2.59",
          "Month": "/Date(1014940800000)/",
          "MonthFormatted": "2002-03-01",
          "Country": 11
      },
      {
          "InflationRate": 3.5699801850378787449450991122,
          "InflationRateRounded": 3.57,
          "InflationRateFormatted": "3.57",
          "Month": "/Date(1012521600000)/",
          "MonthFormatted": "2002-02-01",
          "Country": 11
      },
      {
          "InflationRate": 6.100017485658513026334556207,
          "InflationRateRounded": 6.10,
          "InflationRateFormatted": "6.10",
          "Month": "/Date(1009843200000)/",
          "MonthFormatted": "2002-01-01",
          "Country": 11
      },
      {
          "InflationRate": 5.5299974288171975325782567279,
          "InflationRateRounded": 5.53,
          "InflationRateFormatted": "5.53",
          "Month": "/Date(1007164800000)/",
          "MonthFormatted": "2001-12-01",
          "Country": 11
      },
      {
          "InflationRate": 4.5499887086189460086228469088,
          "InflationRateRounded": 4.55,
          "InflationRateFormatted": "4.55",
          "Month": "/Date(1004572800000)/",
          "MonthFormatted": "2001-11-01",
          "Country": 11
      },
      {
          "InflationRate": 3.5699915458923920727075241558,
          "InflationRateRounded": 3.57,
          "InflationRateFormatted": "3.57",
          "Month": "/Date(1001894400000)/",
          "MonthFormatted": "2001-10-01",
          "Country": 11
      },
      {
          "InflationRate": 2.0500216386654202346624213933,
          "InflationRateRounded": 2.05,
          "InflationRateFormatted": "2.05",
          "Month": "/Date(999302400000)/",
          "MonthFormatted": "2001-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.7599862653084582808744420282,
          "InflationRateRounded": 0.76,
          "InflationRateFormatted": "0.76",
          "Month": "/Date(996624000000)/",
          "MonthFormatted": "2001-08-01",
          "Country": 11
      },
      {
          "InflationRate": 1.5700147167351409628998771166,
          "InflationRateRounded": 1.57,
          "InflationRateFormatted": "1.57",
          "Month": "/Date(993945600000)/",
          "MonthFormatted": "2001-07-01",
          "Country": 11
      },
      {
          "InflationRate": 2.1299967283587970210475584058,
          "InflationRateRounded": 2.13,
          "InflationRateFormatted": "2.13",
          "Month": "/Date(991353600000)/",
          "MonthFormatted": "2001-06-01",
          "Country": 11
      },
      {
          "InflationRate": 2.6300065167983565392731874365,
          "InflationRateRounded": 2.63,
          "InflationRateFormatted": "2.63",
          "Month": "/Date(988675200000)/",
          "MonthFormatted": "2001-05-01",
          "Country": 11
      },
      {
          "InflationRate": 3.2799768066440388507356526068,
          "InflationRateRounded": 3.28,
          "InflationRateFormatted": "3.28",
          "Month": "/Date(986083200000)/",
          "MonthFormatted": "2001-04-01",
          "Country": 11
      },
      {
          "InflationRate": 3.9200211575354829501328642853,
          "InflationRateRounded": 3.92,
          "InflationRateFormatted": "3.92",
          "Month": "/Date(983404800000)/",
          "MonthFormatted": "2001-03-01",
          "Country": 11
      },
      {
          "InflationRate": 3.8799732850559004502695814267,
          "InflationRateRounded": 3.88,
          "InflationRateFormatted": "3.88",
          "Month": "/Date(980985600000)/",
          "MonthFormatted": "2001-02-01",
          "Country": 11
      },
      {
          "InflationRate": 4.7700164969294062159755224086,
          "InflationRateRounded": 4.77,
          "InflationRateFormatted": "4.77",
          "Month": "/Date(978307200000)/",
          "MonthFormatted": "2001-01-01",
          "Country": 11
      },
      {
          "InflationRate": 5.109994355475041447362588325,
          "InflationRateRounded": 5.11,
          "InflationRateFormatted": "5.11",
          "Month": "/Date(975628800000)/",
          "MonthFormatted": "2000-12-01",
          "Country": 11
      },
      {
          "InflationRate": 5.3900179085883360585532975162,
          "InflationRateRounded": 5.39,
          "InflationRateFormatted": "5.39",
          "Month": "/Date(973036800000)/",
          "MonthFormatted": "2000-11-01",
          "Country": 11
      },
      {
          "InflationRate": 5.1599876195669796165013485829,
          "InflationRateRounded": 5.16,
          "InflationRateFormatted": "5.16",
          "Month": "/Date(970358400000)/",
          "MonthFormatted": "2000-10-01",
          "Country": 11
      },
      {
          "InflationRate": 6.7500074296932498177539949898,
          "InflationRateRounded": 6.75,
          "InflationRateFormatted": "6.75",
          "Month": "/Date(967766400000)/",
          "MonthFormatted": "2000-09-01",
          "Country": 11
      },
      {
          "InflationRate": 3.5799827345268268966556859947,
          "InflationRateRounded": 3.58,
          "InflationRateFormatted": "3.58",
          "Month": "/Date(965088000000)/",
          "MonthFormatted": "2000-08-01",
          "Country": 11
      },
      {
          "InflationRate": 4.7000074412202842925319060093,
          "InflationRateRounded": 4.70,
          "InflationRateFormatted": "4.70",
          "Month": "/Date(962409600000)/",
          "MonthFormatted": "2000-07-01",
          "Country": 11
      },
      {
          "InflationRate": 6.1000238362719668343922203236,
          "InflationRateRounded": 6.10,
          "InflationRateFormatted": "6.10",
          "Month": "/Date(959817600000)/",
          "MonthFormatted": "2000-06-01",
          "Country": 11
      },
      {
          "InflationRate": 4.6699623758582283598021732311,
          "InflationRateRounded": 4.67,
          "InflationRateFormatted": "4.67",
          "Month": "/Date(957139200000)/",
          "MonthFormatted": "2000-05-01",
          "Country": 11
      },
      {
          "InflationRate": 5.0599941825163602902548301151,
          "InflationRateRounded": 5.06,
          "InflationRateFormatted": "5.06",
          "Month": "/Date(954547200000)/",
          "MonthFormatted": "2000-04-01",
          "Country": 11
      },
      {
          "InflationRate": 5.780006329221289347780180646,
          "InflationRateRounded": 5.78,
          "InflationRateFormatted": "5.78",
          "Month": "/Date(951868800000)/",
          "MonthFormatted": "2000-03-01",
          "Country": 11
      },
      {
          "InflationRate": 9.250007029706260528582785655,
          "InflationRateRounded": 9.25,
          "InflationRateFormatted": "9.25",
          "Month": "/Date(949363200000)/",
          "MonthFormatted": "2000-02-01",
          "Country": 11
      },
      {
          "InflationRate": 14.109989221890500824766024599,
          "InflationRateRounded": 14.11,
          "InflationRateFormatted": "14.11",
          "Month": "/Date(946684800000)/",
          "MonthFormatted": "2000-01-01",
          "Country": 11
      },
      {
          "InflationRate": 13.650042226194827270414231098,
          "InflationRateRounded": 13.65,
          "InflationRateFormatted": "13.65",
          "Month": "/Date(944006400000)/",
          "MonthFormatted": "1999-12-01",
          "Country": 11
      },
      {
          "InflationRate": 14.310007692686642388598149966,
          "InflationRateRounded": 14.31,
          "InflationRateFormatted": "14.31",
          "Month": "/Date(941414400000)/",
          "MonthFormatted": "1999-11-01",
          "Country": 11
      },
      {
          "InflationRate": 14.199965811891835749942118235,
          "InflationRateRounded": 14.20,
          "InflationRateFormatted": "14.20",
          "Month": "/Date(938736000000)/",
          "MonthFormatted": "1999-10-01",
          "Country": 11
      },
      {
          "InflationRate": 12.139996831003943289044227392,
          "InflationRateRounded": 12.14,
          "InflationRateFormatted": "12.14",
          "Month": "/Date(936144000000)/",
          "MonthFormatted": "1999-09-01",
          "Country": 11
      },
      {
          "InflationRate": 7.0600030835890326583771722969,
          "InflationRateRounded": 7.06,
          "InflationRateFormatted": "7.06",
          "Month": "/Date(933465600000)/",
          "MonthFormatted": "1999-08-01",
          "Country": 11
      },
      {
          "InflationRate": 5.9600070467313901604507705986,
          "InflationRateRounded": 5.96,
          "InflationRateFormatted": "5.96",
          "Month": "/Date(930787200000)/",
          "MonthFormatted": "1999-07-01",
          "Country": 11
      },
      {
          "InflationRate": 7.1199948340238501796149755515,
          "InflationRateRounded": 7.12,
          "InflationRateFormatted": "7.12",
          "Month": "/Date(928195200000)/",
          "MonthFormatted": "1999-06-01",
          "Country": 11
      },
      {
          "InflationRate": 8.85000174921518010657695279,
          "InflationRateRounded": 8.85,
          "InflationRateFormatted": "8.85",
          "Month": "/Date(925516800000)/",
          "MonthFormatted": "1999-05-01",
          "Country": 11
      },
      {
          "InflationRate": 7.4099923174736364549924096915,
          "InflationRateRounded": 7.41,
          "InflationRateFormatted": "7.41",
          "Month": "/Date(922924800000)/",
          "MonthFormatted": "1999-04-01",
          "Country": 11
      },
      {
          "InflationRate": 12.120005117554952838484322463,
          "InflationRateRounded": 12.12,
          "InflationRateFormatted": "12.12",
          "Month": "/Date(920246400000)/",
          "MonthFormatted": "1999-03-01",
          "Country": 11
      },
      {
          "InflationRate": 13.699999736314502966022365804,
          "InflationRateRounded": 13.70,
          "InflationRateFormatted": "13.70",
          "Month": "/Date(917827200000)/",
          "MonthFormatted": "1999-02-01",
          "Country": 11
      },
      {
          "InflationRate": 16.549985453264000721190291795,
          "InflationRateRounded": 16.55,
          "InflationRateFormatted": "16.55",
          "Month": "/Date(915148800000)/",
          "MonthFormatted": "1999-01-01",
          "Country": 11
      },
      {
          "InflationRate": 21.740003423362359164337917667,
          "InflationRateRounded": 21.74,
          "InflationRateFormatted": "21.74",
          "Month": "/Date(912470400000)/",
          "MonthFormatted": "1998-12-01",
          "Country": 11
      },
      {
          "InflationRate": 25.030035684185988129180339627,
          "InflationRateRounded": 25.03,
          "InflationRateFormatted": "25.03",
          "Month": "/Date(909878400000)/",
          "MonthFormatted": "1998-11-01",
          "Country": 11
      },
      {
          "InflationRate": 20.949953912053353575882448625,
          "InflationRateRounded": 20.95,
          "InflationRateFormatted": "20.95",
          "Month": "/Date(907200000000)/",
          "MonthFormatted": "1998-10-01",
          "Country": 11
      },
      {
          "InflationRate": 17.619999356705622947419557702,
          "InflationRateRounded": 17.62,
          "InflationRateFormatted": "17.62",
          "Month": "/Date(904608000000)/",
          "MonthFormatted": "1998-09-01",
          "Country": 11
      },
      {
          "InflationRate": 3.8300180456981105566758142733,
          "InflationRateRounded": 3.83,
          "InflationRateFormatted": "3.83",
          "Month": "/Date(901929600000)/",
          "MonthFormatted": "1998-08-01",
          "Country": 11
      },
      {
          "InflationRate": 2.8299976434449282553321499243,
          "InflationRateRounded": 2.83,
          "InflationRateFormatted": "2.83",
          "Month": "/Date(899251200000)/",
          "MonthFormatted": "1998-07-01",
          "Country": 11
      },
      {
          "InflationRate": 2.6799811530101986533521803231,
          "InflationRateRounded": 2.68,
          "InflationRateFormatted": "2.68",
          "Month": "/Date(896659200000)/",
          "MonthFormatted": "1998-06-01",
          "Country": 11
      },
      {
          "InflationRate": 3.4200141848252550250497977908,
          "InflationRateRounded": 3.42,
          "InflationRateFormatted": "3.42",
          "Month": "/Date(893980800000)/",
          "MonthFormatted": "1998-05-01",
          "Country": 11
      },
      {
          "InflationRate": 3.7599841857934406272446680876,
          "InflationRateRounded": 3.76,
          "InflationRateFormatted": "3.76",
          "Month": "/Date(891388800000)/",
          "MonthFormatted": "1998-04-01",
          "Country": 11
      },
      {
          "InflationRate": 3.2900127764869512714491339483,
          "InflationRateRounded": 3.29,
          "InflationRateFormatted": "3.29",
          "Month": "/Date(888710400000)/",
          "MonthFormatted": "1998-03-01",
          "Country": 11
      },
      {
          "InflationRate": 3.0900419938936918850013372745,
          "InflationRateRounded": 3.09,
          "InflationRateFormatted": "3.09",
          "Month": "/Date(886291200000)/",
          "MonthFormatted": "1998-02-01",
          "Country": 11
      },
      {
          "InflationRate": 3.8700101453102834192726259902,
          "InflationRateRounded": 3.87,
          "InflationRateFormatted": "3.87",
          "Month": "/Date(883612800000)/",
          "MonthFormatted": "1998-01-01",
          "Country": 11
      },
      {
          "InflationRate": 2.3099547310884914201681416713,
          "InflationRateRounded": 2.31,
          "InflationRateFormatted": "2.31",
          "Month": "/Date(880934400000)/",
          "MonthFormatted": "1997-12-01",
          "Country": 11
      },
      {
          "InflationRate": 1.8299856641224931947807482772,
          "InflationRateRounded": 1.83,
          "InflationRateFormatted": "1.83",
          "Month": "/Date(878342400000)/",
          "MonthFormatted": "1997-11-01",
          "Country": 11
      },
      {
          "InflationRate": 3.160031051252403098610991554,
          "InflationRateRounded": 3.16,
          "InflationRateFormatted": "3.16",
          "Month": "/Date(875664000000)/",
          "MonthFormatted": "1997-10-01",
          "Country": 11
      },
      {
          "InflationRate": 4.9600136747127699840054698851,
          "InflationRateRounded": 4.96,
          "InflationRateFormatted": "4.96",
          "Month": "/Date(873072000000)/",
          "MonthFormatted": "1997-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.9699665418251841185854366945,
          "InflationRateRounded": 0.97,
          "InflationRateFormatted": "0.97",
          "Month": "/Date(870393600000)/",
          "MonthFormatted": "1997-08-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3800429935509673548967654852,
          "InflationRateRounded": 1.38,
          "InflationRateFormatted": "1.38",
          "Month": "/Date(867715200000)/",
          "MonthFormatted": "1997-07-01",
          "Country": 11
      },
      {
          "InflationRate": 4.4799506928593440687792826438,
          "InflationRateRounded": 4.48,
          "InflationRateFormatted": "4.48",
          "Month": "/Date(865123200000)/",
          "MonthFormatted": "1997-06-01",
          "Country": 11
      },
      {
          "InflationRate": 4.9800543763851127510338367338,
          "InflationRateRounded": 4.98,
          "InflationRateFormatted": "4.98",
          "Month": "/Date(862444800000)/",
          "MonthFormatted": "1997-05-01",
          "Country": 11
      },
      {
          "InflationRate": 4.2599636403828050263168927391,
          "InflationRateRounded": 4.26,
          "InflationRateFormatted": "4.26",
          "Month": "/Date(859852800000)/",
          "MonthFormatted": "1997-04-01",
          "Country": 11
      },
      {
          "InflationRate": 2.2699788836907449759847050489,
          "InflationRateRounded": 2.27,
          "InflationRateFormatted": "2.27",
          "Month": "/Date(857174400000)/",
          "MonthFormatted": "1997-03-01",
          "Country": 11
      },
      {
          "InflationRate": 6.6300240395805209573272508429,
          "InflationRateRounded": 6.63,
          "InflationRateFormatted": "6.63",
          "Month": "/Date(854755200000)/",
          "MonthFormatted": "1997-02-01",
          "Country": 11
      },
      {
          "InflationRate": 13.269996819914163575047318783,
          "InflationRateRounded": 13.27,
          "InflationRateFormatted": "13.27",
          "Month": "/Date(852076800000)/",
          "MonthFormatted": "1997-01-01",
          "Country": 11
      },
      {
          "InflationRate": 7.3999917585174836984234246191,
          "InflationRateRounded": 7.40,
          "InflationRateFormatted": "7.40",
          "Month": "/Date(849398400000)/",
          "MonthFormatted": "1996-12-01",
          "Country": 11
      },
      {
          "InflationRate": 3.8900062048792018358563225749,
          "InflationRateRounded": 3.89,
          "InflationRateFormatted": "3.89",
          "Month": "/Date(846806400000)/",
          "MonthFormatted": "1996-11-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3399946022801374023127153565,
          "InflationRateRounded": 1.34,
          "InflationRateFormatted": "1.34",
          "Month": "/Date(844128000000)/",
          "MonthFormatted": "1996-10-01",
          "Country": 11
      },
      {
          "InflationRate": 1.820007891633134452615512696,
          "InflationRateRounded": 1.82,
          "InflationRateFormatted": "1.82",
          "Month": "/Date(841536000000)/",
          "MonthFormatted": "1996-09-01",
          "Country": 11
      },
      {
          "InflationRate": 1.3400007470081226147920790774,
          "InflationRateRounded": 1.34,
          "InflationRateFormatted": "1.34",
          "Month": "/Date(838857600000)/",
          "MonthFormatted": "1996-08-01",
          "Country": 11
      },
      {
          "InflationRate": 2.0399916264922316659273057342,
          "InflationRateRounded": 2.04,
          "InflationRateFormatted": "2.04",
          "Month": "/Date(836179200000)/",
          "MonthFormatted": "1996-07-01",
          "Country": 11
      },
      {
          "InflationRate": 2.290003468529544132021980731,
          "InflationRateRounded": 2.29,
          "InflationRateFormatted": "2.29",
          "Month": "/Date(833587200000)/",
          "MonthFormatted": "1996-06-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6400150014785111032577730017,
          "InflationRateRounded": 0.64,
          "InflationRateFormatted": "0.64",
          "Month": "/Date(830908800000)/",
          "MonthFormatted": "1996-05-01",
          "Country": 11
      },
      {
          "InflationRate": 1.5199919400801188838182464637,
          "InflationRateRounded": 1.52,
          "InflationRateFormatted": "1.52",
          "Month": "/Date(828316800000)/",
          "MonthFormatted": "1996-04-01",
          "Country": 11
      },
      {
          "InflationRate": 1.9799990024291149724338907684,
          "InflationRateRounded": 1.98,
          "InflationRateFormatted": "1.98",
          "Month": "/Date(825638400000)/",
          "MonthFormatted": "1996-03-01",
          "Country": 11
      },
      {
          "InflationRate": 3.9999888176497246812175964221,
          "InflationRateRounded": 4.00,
          "InflationRateFormatted": "4.00",
          "Month": "/Date(823132800000)/",
          "MonthFormatted": "1996-02-01",
          "Country": 11
      },
      {
          "InflationRate": 5.5600221654605726951691755826,
          "InflationRateRounded": 5.56,
          "InflationRateFormatted": "5.56",
          "Month": "/Date(820454400000)/",
          "MonthFormatted": "1996-01-01",
          "Country": 11
      },
      {
          "InflationRate": 3.8799977383256388719149773935,
          "InflationRateRounded": 3.88,
          "InflationRateFormatted": "3.88",
          "Month": "/Date(817776000000)/",
          "MonthFormatted": "1995-12-01",
          "Country": 11
      },
      {
          "InflationRate": 3.6799799976904112868964742751,
          "InflationRateRounded": 3.68,
          "InflationRateFormatted": "3.68",
          "Month": "/Date(815184000000)/",
          "MonthFormatted": "1995-11-01",
          "Country": 11
      },
      {
          "InflationRate": 3.4200111613012557924816072274,
          "InflationRateRounded": 3.42,
          "InflationRateFormatted": "3.42",
          "Month": "/Date(812505600000)/",
          "MonthFormatted": "1995-10-01",
          "Country": 11
      },
      {
          "InflationRate": 5.2399908828947808416256582708,
          "InflationRateRounded": 5.24,
          "InflationRateFormatted": "5.24",
          "Month": "/Date(809913600000)/",
          "MonthFormatted": "1995-09-01",
          "Country": 11
      },
      {
          "InflationRate": 2.9500006331237693656732954725,
          "InflationRateRounded": 2.95,
          "InflationRateFormatted": "2.95",
          "Month": "/Date(807235200000)/",
          "MonthFormatted": "1995-08-01",
          "Country": 11
      },
      {
          "InflationRate": 5.2000105734845730154266723641,
          "InflationRateRounded": 5.20,
          "InflationRateFormatted": "5.20",
          "Month": "/Date(804556800000)/",
          "MonthFormatted": "1995-07-01",
          "Country": 11
      },
      {
          "InflationRate": 2.529984519575459920553453987,
          "InflationRateRounded": 2.53,
          "InflationRateFormatted": "2.53",
          "Month": "/Date(801964800000)/",
          "MonthFormatted": "1995-06-01",
          "Country": 11
      },
      {
          "InflationRate": 3.4200272878274307552619114986,
          "InflationRateRounded": 3.42,
          "InflationRateFormatted": "3.42",
          "Month": "/Date(799286400000)/",
          "MonthFormatted": "1995-05-01",
          "Country": 11
      },
      {
          "InflationRate": 14.449980777478985313589791907,
          "InflationRateRounded": 14.45,
          "InflationRateFormatted": "14.45",
          "Month": "/Date(796694400000)/",
          "MonthFormatted": "1995-04-01",
          "Country": 11
      },
      {
          "InflationRate": 19.990022295260044687508713822,
          "InflationRateRounded": 19.99,
          "InflationRateFormatted": "19.99",
          "Month": "/Date(794016000000)/",
          "MonthFormatted": "1995-03-01",
          "Country": 11
      },
      {
          "InflationRate": 33.669977332439325280508531227,
          "InflationRateRounded": 33.67,
          "InflationRateFormatted": "33.67",
          "Month": "/Date(791596800000)/",
          "MonthFormatted": "1995-02-01",
          "Country": 11
      },
      {
          "InflationRate": 39.189971809954317351455263881,
          "InflationRateRounded": 39.19,
          "InflationRateFormatted": "39.19",
          "Month": "/Date(788918400000)/",
          "MonthFormatted": "1995-01-01",
          "Country": 11
      },
      {
          "InflationRate": 31.320049674869536819608688215,
          "InflationRateRounded": 31.32,
          "InflationRateFormatted": "31.32",
          "Month": "/Date(786240000000)/",
          "MonthFormatted": "1994-12-01",
          "Country": 11
      },
      {
          "InflationRate": 40.529969030730228094879460867,
          "InflationRateRounded": 40.53,
          "InflationRateFormatted": "40.53",
          "Month": "/Date(783648000000)/",
          "MonthFormatted": "1994-11-01",
          "Country": 11
      },
      {
          "InflationRate": 25.690001819451545886345624376,
          "InflationRateRounded": 25.69,
          "InflationRateFormatted": "25.69",
          "Month": "/Date(780969600000)/",
          "MonthFormatted": "1994-10-01",
          "Country": 11
      },
      {
          "InflationRate": 25.539995346332839668029651446,
          "InflationRateRounded": 25.54,
          "InflationRateFormatted": "25.54",
          "Month": "/Date(778377600000)/",
          "MonthFormatted": "1994-09-01",
          "Country": 11
      },
      {
          "InflationRate": 53.440017173692905848806496373,
          "InflationRateRounded": 53.44,
          "InflationRateFormatted": "53.44",
          "Month": "/Date(775699200000)/",
          "MonthFormatted": "1994-08-01",
          "Country": 11
      },
      {
          "InflationRate": 26.640004007695029724269943787,
          "InflationRateRounded": 26.64,
          "InflationRateFormatted": "26.64",
          "Month": "/Date(773020800000)/",
          "MonthFormatted": "1994-07-01",
          "Country": 11
      },
      {
          "InflationRate": 19.539969020018743613382650066,
          "InflationRateRounded": 19.54,
          "InflationRateFormatted": "19.54",
          "Month": "/Date(770428800000)/",
          "MonthFormatted": "1994-06-01",
          "Country": 11
      },
      {
          "InflationRate": 28.719999253709725487924090496,
          "InflationRateRounded": 28.72,
          "InflationRateFormatted": "28.72",
          "Month": "/Date(767750400000)/",
          "MonthFormatted": "1994-05-01",
          "Country": 11
      },
      {
          "InflationRate": 28.580023307698232225986189621,
          "InflationRateRounded": 28.58,
          "InflationRateFormatted": "28.58",
          "Month": "/Date(765158400000)/",
          "MonthFormatted": "1994-04-01",
          "Country": 11
      },
      {
          "InflationRate": 10.200012522523149275432896668,
          "InflationRateRounded": 10.20,
          "InflationRateFormatted": "10.20",
          "Month": "/Date(762480000000)/",
          "MonthFormatted": "1994-03-01",
          "Country": 11
      },
      {
          "InflationRate": 18.659962406170209077498268488,
          "InflationRateRounded": 18.66,
          "InflationRateFormatted": "18.66",
          "Month": "/Date(760060800000)/",
          "MonthFormatted": "1994-02-01",
          "Country": 11
      },
      {
          "InflationRate": 40.699963645191002126698251601,
          "InflationRateRounded": 40.70,
          "InflationRateFormatted": "40.70",
          "Month": "/Date(757382400000)/",
          "MonthFormatted": "1994-01-01",
          "Country": 11
      },
      {
          "InflationRate": 45.500098018670866823045879498,
          "InflationRateRounded": 45.50,
          "InflationRateFormatted": "45.50",
          "Month": "/Date(754704000000)/",
          "MonthFormatted": "1993-12-01",
          "Country": 11
      },
      {
          "InflationRate": 43.159840418453975143408067626,
          "InflationRateRounded": 43.16,
          "InflationRateFormatted": "43.16",
          "Month": "/Date(752112000000)/",
          "MonthFormatted": "1993-11-01",
          "Country": 11
      },
      {
          "InflationRate": 44.550137614839321123440685731,
          "InflationRateRounded": 44.55,
          "InflationRateFormatted": "44.55",
          "Month": "/Date(749433600000)/",
          "MonthFormatted": "1993-10-01",
          "Country": 11
      },
      {
          "InflationRate": 35.980083982062268466780481542,
          "InflationRateRounded": 35.98,
          "InflationRateFormatted": "35.98",
          "Month": "/Date(746841600000)/",
          "MonthFormatted": "1993-09-01",
          "Country": 11
      },
      {
          "InflationRate": 25.120042840567637521197155862,
          "InflationRateRounded": 25.12,
          "InflationRateFormatted": "25.12",
          "Month": "/Date(744163200000)/",
          "MonthFormatted": "1993-08-01",
          "Country": 11
      },
      {
          "InflationRate": 23.250049500957018502357712249,
          "InflationRateRounded": 23.25,
          "InflationRateFormatted": "23.25",
          "Month": "/Date(741484800000)/",
          "MonthFormatted": "1993-07-01",
          "Country": 11
      },
      {
          "InflationRate": 26.249664379820200168504476479,
          "InflationRateRounded": 26.25,
          "InflationRateFormatted": "26.25",
          "Month": "/Date(738892800000)/",
          "MonthFormatted": "1993-06-01",
          "Country": 11
      },
      {
          "InflationRate": 19.660325936430209498908744447,
          "InflationRateRounded": 19.66,
          "InflationRateFormatted": "19.66",
          "Month": "/Date(736214400000)/",
          "MonthFormatted": "1993-05-01",
          "Country": 11
      },
      {
          "InflationRate": 24.800210159555346625003456572,
          "InflationRateRounded": 24.80,
          "InflationRateFormatted": "24.80",
          "Month": "/Date(733622400000)/",
          "MonthFormatted": "1993-04-01",
          "Country": 11
      },
      {
          "InflationRate": 28.128543083900226757369614512,
          "InflationRateRounded": 28.13,
          "InflationRateFormatted": "28.13",
          "Month": "/Date(730944000000)/",
          "MonthFormatted": "1993-03-01",
          "Country": 11
      },
      {
          "InflationRate": 19.720042417815482502651113468,
          "InflationRateRounded": 19.72,
          "InflationRateFormatted": "19.72",
          "Month": "/Date(728524800000)/",
          "MonthFormatted": "1993-02-01",
          "Country": 11
      },
      {
          "InflationRate": 14.820767582310539645431521527,
          "InflationRateRounded": 14.82,
          "InflationRateFormatted": "14.82",
          "Month": "/Date(725846400000)/",
          "MonthFormatted": "1993-01-01",
          "Country": 11
      },
      {
          "InflationRate": 30.710466004583651642475171887,
          "InflationRateRounded": 30.71,
          "InflationRateFormatted": "30.71",
          "Month": "/Date(723168000000)/",
          "MonthFormatted": "1992-12-01",
          "Country": 11
      },
      {
          "InflationRate": 21.049589642815859438215235233,
          "InflationRateRounded": 21.05,
          "InflationRateFormatted": "21.05",
          "Month": "/Date(720576000000)/",
          "MonthFormatted": "1992-11-01",
          "Country": 11
      },
      {
          "InflationRate": 12.599245086554731224781986203,
          "InflationRateRounded": 12.60,
          "InflationRateFormatted": "12.60",
          "Month": "/Date(717897600000)/",
          "MonthFormatted": "1992-10-01",
          "Country": 11
      },
      {
          "InflationRate": 8.819224776922713752891742599,
          "InflationRateRounded": 8.82,
          "InflationRateFormatted": "8.82",
          "Month": "/Date(715305600000)/",
          "MonthFormatted": "1992-09-01",
          "Country": 11
      },
      {
          "InflationRate": 8.832596855410543623471380125,
          "InflationRateRounded": 8.83,
          "InflationRateFormatted": "8.83",
          "Month": "/Date(712627200000)/",
          "MonthFormatted": "1992-08-01",
          "Country": 11
      },
      {
          "InflationRate": 13.046003717472118959107806691,
          "InflationRateRounded": 13.05,
          "InflationRateFormatted": "13.05",
          "Month": "/Date(709948800000)/",
          "MonthFormatted": "1992-07-01",
          "Country": 11
      },
      {
          "InflationRate": 11.444847229414810978767477991,
          "InflationRateRounded": 11.44,
          "InflationRateFormatted": "11.44",
          "Month": "/Date(707356800000)/",
          "MonthFormatted": "1992-06-01",
          "Country": 11
      },
      {
          "InflationRate": 15.776062354792775237952484449,
          "InflationRateRounded": 15.78,
          "InflationRateFormatted": "15.78",
          "Month": "/Date(704678400000)/",
          "MonthFormatted": "1992-05-01",
          "Country": 11
      },
      {
          "InflationRate": 16.01599860881662464133553604,
          "InflationRateRounded": 16.02,
          "InflationRateFormatted": "16.02",
          "Month": "/Date(702086400000)/",
          "MonthFormatted": "1992-04-01",
          "Country": 11
      },
      {
          "InflationRate": 19.37928171060826240398588333,
          "InflationRateRounded": 19.38,
          "InflationRateFormatted": "19.38",
          "Month": "/Date(699408000000)/",
          "MonthFormatted": "1992-03-01",
          "Country": 11
      },
      {
          "InflationRate": 50.507733166692704264958600219,
          "InflationRateRounded": 50.51,
          "InflationRateFormatted": "50.51",
          "Month": "/Date(696902400000)/",
          "MonthFormatted": "1992-02-01",
          "Country": 11
      },
      {
          "InflationRate": 158.62626262626262626262626263,
          "InflationRateRounded": 158.63,
          "InflationRateFormatted": "158.63",
          "Month": "/Date(694224000000)/",
          "MonthFormatted": "1992-01-01",
          "Country": 11
      },
      {
          "InflationRate": 10.738255033557046979865771812,
          "InflationRateRounded": 10.74,
          "InflationRateFormatted": "10.74",
          "Month": "/Date(691545600000)/",
          "MonthFormatted": "1991-12-01",
          "Country": 11
      },
      {
          "InflationRate": 6.6825775656324582338902147971,
          "InflationRateRounded": 6.68,
          "InflationRateFormatted": "6.68",
          "Month": "/Date(688953600000)/",
          "MonthFormatted": "1991-11-01",
          "Country": 11
      },
      {
          "InflationRate": 3.7128712871287128712871287129,
          "InflationRateRounded": 3.71,
          "InflationRateFormatted": "3.71",
          "Month": "/Date(686275200000)/",
          "MonthFormatted": "1991-10-01",
          "Country": 11
      },
      {
          "InflationRate": 0.6979062811565304087736789631,
          "InflationRateRounded": 0.70,
          "InflationRateFormatted": "0.70",
          "Month": "/Date(683683200000)/",
          "MonthFormatted": "1991-09-01",
          "Country": 11
      },
      {
          "InflationRate": 0.4004004004004004004004004004,
          "InflationRateRounded": 0.40,
          "InflationRateFormatted": "0.40",
          "Month": "/Date(681004800000)/",
          "MonthFormatted": "1991-08-01",
          "Country": 11
      },
      {
          "InflationRate": 1.9387755102040816326530612245,
          "InflationRateRounded": 1.94,
          "InflationRateFormatted": "1.94",
          "Month": "/Date(678326400000)/",
          "MonthFormatted": "1991-07-01",
          "Country": 11
      },
      {
          "InflationRate": 2.5104602510460251046025104603,
          "InflationRateRounded": 2.51,
          "InflationRateFormatted": "2.51",
          "Month": "/Date(675734400000)/",
          "MonthFormatted": "1991-06-01",
          "Country": 11
      },
      {
          "InflationRate": 3.3513513513513513513513513514,
          "InflationRateRounded": 3.35,
          "InflationRateFormatted": "3.35",
          "Month": "/Date(673056000000)/",
          "MonthFormatted": "1991-05-01",
          "Country": 11
      },
      {
          "InflationRate": 50.651465798045602605863192182,
          "InflationRateRounded": 50.65,
          "InflationRateFormatted": "50.65",
          "Month": "/Date(670464000000)/",
          "MonthFormatted": "1991-04-01",
          "Country": 11
      },
      {
          "InflationRate": 4.0677966101694915254237288136,
          "InflationRateRounded": 4.07,
          "InflationRateFormatted": "4.07",
          "Month": "/Date(667785600000)/",
          "MonthFormatted": "1991-03-01",
          "Country": 11
      },
      {
          "InflationRate": 11.42587346553352219074598678,
          "InflationRateRounded": 11.43,
          "InflationRateFormatted": "11.43",
          "Month": "/Date(665366400000)/",
          "MonthFormatted": "1991-02-01",
          "Country": 11
      },
      {
          "InflationRate": 5.9,
          "InflationRateRounded": 5.9,
          "InflationRateFormatted": "5.90",
          "Month": "/Date(662688000000)/",
          "MonthFormatted": "1991-01-01",
          "Country": 11
      }
    ]
    this.chartData = [...this.chartData,...this.newCountryData ];
  }

  filterChartData(){
    let filterdata = {
      start: `${this.filterForm.value.startDate.year}-${this.filterForm.value.startDate.month}-${this.filterForm.value.startDate.day}`,
      end:`${this.filterForm.value.endDate.year}-${this.filterForm.value.endDate.month}-${this.filterForm.value.endDate.day}`,
      country:this.filterForm.value.country
    };
    
    if(filterdata.country.length == 1){
      this.lineChartData = this.lineChartData.slice(0,1);
    }else{
      if (this.lineChartData.length != 2){
        this.lineChartData.push({data: [], label: ""})
      }
    }
    let countryData = []
    filterdata.country.forEach((element, index) => {
      countryData = this.chartData.filter((item)=> item.Country === parseInt(element) && item.MonthFormatted >= filterdata.start.trim() && item.MonthFormatted <=filterdata.end.trim()).slice(0, 12)
      this.lineChartData[index].data = countryData.map(function(item){
        return item.InflationRateRounded
      })
      this.lineChartData[index].label = Object.keys(this.countries).filter((item)=> this.countries[item] == element && item).toString()
    });
   
    this.lineChartLabels = countryData.map(function(item){
      return item.MonthFormatted
    })
  }
}
